<?php

class StatsCommand extends CConsoleCommand {
    public function run($args) 
    {
        $criteria = new CDbCriteria;
        $criteria->order = 'date desc';
        $statistics = Statistics::model()->find($criteria);
        if (isset($statistics->date)) {
            if ($statistics->date != date('Y-m-d')) {
                $stats = new Statistics;
                $stats->user = 0;
                $stats->moneyUp = 0;
                $stats->moneyDown = 0;
                $stats->date = date('Y-m-d');
                $stats->save();
            }
        } else {
            $stats = new Statistics;
            $stats->user = 0;
            $stats->moneyUp = 0;
            $stats->moneyDown = 0;
            $stats->date = date('Y-m-d');
            $stats->save();
        }
    }
}
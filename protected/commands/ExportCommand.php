<?php

class ExportCommand extends CConsoleCommand
{
	public function run($args)
	{
		$model = Settings::model()->find('idUser = :id', array(':id'=>$args[0]));
        if ($model->dateXls!=0&&strtotime($model->dateXls)<strtotime(date('Y-m-d H:i:s')))
        {   
        	if (file_exists('/home/wwwroot/clientfinder.ru/export/'.$model->nameXls))
        	{
	        	unlink('/home/wwwroot/clientfinder.ru/export/'.$model->nameXls);
	        	$model->dateXls=NULL;
	        	$model->nameXls=NULL;
	        	$model->save();
	        	exit();
            }

        } 

         if ($model->dateTxt!=0&&strtotime($model->dateTxt)<strtotime(date('Y-m-d H:i:s')))
        {   
        	if (file_exists('/home/wwwroot/clientfinder.ru/export/'.$model->nameTxt))
        	{
	        	unlink('/home/wwwroot/clientfinder.ru/export/'.$model->nameTxt);
	        	$model->dateTxt=NULL;
	        	$model->nameTxt=NULL;
	        	$model->save();
	        	exit();
            }

        }
    }
}

?>
<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'My Web Application',

	// preloading 'log' component
	'preload'=>array('log'),

	// autoloading model and component classes
	'import'=>array(
		'application.helpers.*',
		'application.models.*',
		'application.components.*',
	),

	

	// application components
	'components'=>array(

		

		// uncomment the following to enable URLs in path-format
		
		
		

		// database settings are configured in database.php
		// 'db'=>require(dirname(__FILE__).'/database.php'),
		'db'=>array(
            'connectionString' => 'mysql:host=localhost;dbname=vk',
            'emulatePrepare' => true,
            'username' => 'root',
            'password' => 'dj5$DK9%aS',
            'charset' => 'utf8',
        ),

		// 'errorHandler'=>array(
		// 	// use 'site/error' action to display errors
		// 	'errorAction'=>'site/error',
		// ),

		
	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		// this is used in contact page
		'vk_application_id'=>'4943467',
		
		'vk_client_secret'=>'roYwxayQVyk6T8K9zLMO',
		'path'=>'/home/wwwroot/clientfinder.ru'
	),
);

<?php
//Yii::setPathOfAlias('framework', '/home/wwwroot/bak/framework');

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name' => 'My Web Application',

    // preloading 'log' component
    'preload' => array('log'),

    // autoloading model and component classes
    'import' => array(
        'application.helpers.*',
        'application.models.*',
        'application.modules.*',
        'application.components.*',
    ),

    'modules' => array(
        // uncomment the following to enable the Gii tool
        'user',
        'ticket',
        'gii' => array(
            'class' => 'system.gii.GiiModule',
            'password' => '321',
            // If removed, Gii defaults to localhost only. Edit carefully to taste.
            'ipFilters' => array('88.86.82.189', '::1'),
        ),


    ),

    // application components
    'components' => array(

        'user' => array(
            // enable cookie-based authentication
            'allowAutoLogin' => true,
            'behaviors' => array(
                'userBehavior' => array(
                    'class' => 'application.modules.user.components.UserBehavior',
                ),
            ),
        ),

        // uncomment the following to enable URLs in path-format

        'urlManager' => array(
            'showScriptName' => false,
            'urlFormat' => 'path',
            'rules' => array(
                'admin/user/<user:view|edit|delete>/id/<id:[\d]+>' => 'user/admin/user',
                'admin/user' => 'user/admin/user',
                'admin/ticket' => 'ticket/list/',
                'admin/ticket/new' => 'ticket/list/index/act/new/',
//                'admin/ticket/act/view/<view:[\d]+>'=>'ticket/list/index',
                'success' => 'site/success',
                'fail' => 'site/fail',
                'payment' => 'site/payment',
                '<controller:\w+>/<id:\d+>' => '<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
                'logout' => 'site/logout',
            ),
        ),


        // database settings are configured in database.php
        // 'db'=>require(dirname(__FILE__).'/database.php'),
        'db' => array(
            'connectionString' => 'mysql:host=localhost;dbname=vk',
            'emulatePrepare' => true,
            'username' => 'root',
            'password' => 'dj5$DK9%aS',
            'charset' => 'utf8',
        ),

        // 'errorHandler'=>array(
        // 	// use 'site/error' action to display errors
        // 	'errorAction'=>'site/error',
        // ),

        'log' => array(
            'class' => 'CLogRouter',
            'routes' => array(
                array(
                    'class' => 'CFileLogRoute',
                    'levels' => 'error, warning',
                ),
                // uncomment the following to show log messages on web pages
                /*
                array(
                    'class'=>'CWebLogRoute',
                ),
                */
            ),
        ),
    ),

    // application-level parameters that can be accessed
    // using Yii::app()->params['paramName']
    'params' => array(
        // this is used in contact page
        'typeSubs' => array(
            0 => 'Подписка отключена',
            1 => 'Тестовый период',
            2 => 'Подписка на 1 месяц'
        ),
        'type' => array(
            0 => 'Списано',
            1 => 'Внесено'
        ),
        'type_pay' => array(
            0 => 'WebMoney',
            1 => 'Yandex Деньги',
            2 => 'Robokassa',
            3 => 'Ручное пополнение',
            4 => 'Ручное списание',
            5 => 'Личный счет',
            6 => 'Бонус ПП'
        ),
        'yandex' => array(
            'secret' => 'AOhSGtIq3+FyfXRn/ihg4wCB',
            'token' => '2EB10BD062F0E06EEC6CC6F1FFC553CE9C70566490B9D9E444499A4CCB96D0871336B353EC12DD1FA94EBCC1B1112E72A0F5DDDD4090A736440C0E79AC1C8C80808580184370272971DA8D6AB769A7F5CA77CD722A06DFD874C2F1EAABB23153FE481F1707B6E3E803412BD55F5167515FE7B897A5B76E7DCAC5105C0A8DE7A0'
        ),
        'openpp' => array(
            'secret_key' => 'IrMZkLX5hewTb8x9ygdG'
        ),
        'vk_application_id' => '5005132',
        'path' => '/home/wwwroot/clientfinder.ru',
        'vk_client_secret' => 'Qq3vsEW8sea5M39ekZUY',
        'robokassa' => array(
            0 => 'socsearch',
            1 => 'uWzVAxt7ie',
            2 => 'g9xfCJxAES'
        )
    ),
);

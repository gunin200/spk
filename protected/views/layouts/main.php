<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <base href="<?= Yii::app()->request->getBaseUrl(true) ?>">
    <title>Сервис поиска клиентов SocSearch.info</title>
    <!-- BOOTSTRAP STYLES-->
    <link href="assets/admin/css/bootstrap.css" rel="stylesheet"/>
    <link href="assets/admin/css/form.css" rel="stylesheet"/>
    <!-- FONTAWESOME STYLES-->
    <link href="assets/admin/css/font-awesome.css" rel="stylesheet"/>
    <!-- MORRIS CHART STYLES-->

    <!-- CUSTOM STYLES-->
    <link href="assets/admin/css/custom.css" rel="stylesheet"/>
    <!-- GOOGLE FONTS-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'/>
</head>
<body>

<div id="wrapper">
    <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?= Yii::app()->getRequest()->getBaseUrl(true); ?>"><?= User::Data('name') ?></a>
        </div>
        <div style="color: white;
padding: 15px 50px 5px 50px;
float: right;
font-size: 16px;"><?= User::Data('balance') ?> <i class="fa fa-rub">&nbsp;</i>
            <a href="<?= Yii::app()->request->getBaseUrl(true) ?>/logout" class="btn btn-danger square-btn-adjust">Выход</a></div>
    </nav>
    <!-- /. NAV TOP  -->
    <nav class="navbar-default navbar-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav" id="main-menu">
                <li class="text-center">
                    <? if (isset(Yii::app()->session['urlPhoto'])) { ?>
                        <img src="<?= Yii::app()->session['urlPhoto'] ?>" class="user-image img-responsive"/>
                    <? } else { ?>
                        <img src="assets/admin/img/find_user.png" class="user-image img-responsive">
                    <? } ?>
                </li>
                <li>
                    <a class="active-menu" href="<?= Yii::app()->request->getBaseUrl(true) ?>">
                        <div class='icon-menu'><i class="fa fa-search fa-3x"></i></div>
                        <div class="name-menu">Поиск по постам</div>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <div class='icon-menu'><i class="fa fa-sitemap fa-3x"></i></div>
                        <div class="name-menu"><label>Список групп</label><span class="fa arrow"></span></div>
                    </a>
                    <ul class="nav nav-second-level">
                        <? if (User::isPremium()) { ?>
                        <?= UserGroup::listGroup() ?>
                        <? } ?>
                        <li style="clear:both;margin-left:-1px;">
                            <? if (User::isPremium()) { ?>
                            <a href="#" class="btn" data-toggle="modal" data-target=".bs-example-modal-lg">Добавить группу</a>
                            <? } else { ?>
                                <a href="/payment" class="btn">Добавить группу</a>
                            <? } ?>
                        </li>

                </li>
            </ul>
            <li>
                <a href="<?= Yii::app()->getRequest()->getBaseUrl(true) ?>/payment">
                    <div class='icon-menu'><i class="fa fa-rub fa-3x"></i></div>
                    <div class="name-menu"><label>Пополнить баланс</label></div>
                </a>
            </li>
            <li>
                <a href="<?= Yii::app()->getRequest()->getBaseUrl(true) ?>/ticket">
                    <div class='icon-menu'><i class="fa fa-ambulance fa-3x"></i></div>
                    <div class="name-menu"><label>Тикеты</label></div>
                </a>
            </li>
            <li>
                <a href="#">
                    <div class='icon-menu'><i class="fa fa-question fa-3x"></i></div>
                    <div class="name-menu"><label>FAQ</label></div>
                </a>
            </li>
            <li>
                <a href="#">
                    <div class='icon-menu'><i class="fa fa-sign-out fa-3x"></i></div>
                    <div class="name-menu"><label>Выход</label></div>
                </a>
            </li>
        </div>
    </nav>

    <div id="page-wrapper">
        <div id="page-inner">
            <div class="row">
                <?= $content ?>
            </div>
        </div>
    </div>

    <!-- BOOTSTRAP SCRIPTS -->
    <script src="assets/admin/js/bootstrap.min.js"></script>
    <!-- METISMENU SCRIPTS -->
    <script src="assets/admin/js/jquery.metisMenu.js"></script>
    <!-- MORRIS CHART SCRIPTS -->

    <!-- CUSTOM SCRIPTS -->
    <script src="assets/admin/js/custom.js"></script>

</body>
</html>

<script type="text/javascript" src="http://cdn.jsdelivr.net/momentjs/2.9.0/moment.min.js"></script>
<script type="text/javascript" src="<?= Yii::app()->request->getBaseUrl(true) ?>/assets/bootstrap/js/daterangepicker/daterangepicker.js"></script>
<link rel="stylesheet" type="text/css" href="<?= Yii::app()->request->getBaseUrl(true) ?>/assets/bootstrap/js/daterangepicker/daterangepicker-bs3.css"/>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Статистика</h1>
    </div>
</div>
<?$this->widget('zii.widgets.CBreadcrumbs', array(
    'htmlOptions'=>array(
        'class'=>'breadcrumb'
    ),
    'separator'=>' / ',
    'links'=>array(
        'Администрирование'=>array('../admin'),
        'Статистика'
    )
));?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'pay-grid',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'itemsCssClass'=>'table table-hover',
    'summaryText'=>'',
    'emptyText'=>'Нет статистики',
    'afterAjaxUpdate' => "
        function() {
            jQuery('#Statistics_date').daterangepicker();
        }",
    'columns' => array(
        array(
            'name' => 'user',
            'value' => 'Statistics::getInfo($data->date)',
            'htmlOptions' => array(
                'style' => 'text-align: center',
            ),
        ),
        array(
            'name' => 'moneyUp',
            'htmlOptions' => array(
                'style' => 'text-align: center',
            ),
        ),
        array(
            'name' => 'moneyDown',
            'htmlOptions' => array(
                'style' => 'text-align: center',
            ),
        ),
        array(
            'name' => 'date',
            'htmlOptions' => array(
                'style' => 'text-align: center',
            ),
            'filter' => CHtml::textField("Statistics[date]", $model->date),
        ),
    )
)); ?>

<script type="text/javascript">
    $(function () {
        $('#Statistics_date').daterangepicker();
    });
</script>
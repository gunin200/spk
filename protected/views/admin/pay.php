<script type="text/javascript" src="http://cdn.jsdelivr.net/momentjs/2.9.0/moment.min.js"></script>
<script type="text/javascript" src="<?= Yii::app()->request->getBaseUrl(true) ?>/assets/bootstrap/js/daterangepicker/daterangepicker.js"></script>
<link rel="stylesheet" type="text/css" href="<?= Yii::app()->request->getBaseUrl(true) ?>/assets/bootstrap/js/daterangepicker/daterangepicker-bs3.css"/>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Платежи</h1>

        <?$this->widget('zii.widgets.CBreadcrumbs', array(
            'htmlOptions' => array(
                'class' => 'breadcrumb'
            ),
            'separator' => ' / ',
            'links' => array(
                'Администрирование' => array('../admin'),
                'Платежи'
            )
        ));?>

        <?php $this->widget('zii.widgets.grid.CGridView', array(
            'id' => 'pay-grid',
            'dataProvider' => $model->search(),
            'filter' => $model,
            'itemsCssClass' => 'table table-hover',
            'summaryText' => '',
            'emptyText' => 'Нет платежей',
            'afterAjaxUpdate' => "
                function() {
                    jQuery('#Pay_date').daterangepicker();
                }",
            'columns' => array(
                array(
                    'name' => 'user_id',
                    'value' => '$data->user->userFirstName." ".$data->user->userLastName',
                    'htmlOptions' => array(
                        'style' => 'text-align: center',
                    ),
                ),
                array(
                    'name' => 'type',
                    'value' => 'Yii::app()->params["type"][$data->type]',
                    'filter' => array('' => 'Любая', 0 => 'Списано', 1 => 'Внесено'),
                    'htmlOptions' => array(
                        'style' => 'text-align: center',
                    ),
                    'cssClassExpression' => '$data->type == 0 ? "red-text" : "green-text"',
                ),
                array(
                    'name' => 'type_pay',
                    'value' => 'Yii::app()->params["type_pay"][$data->type_pay]',
                    'filter' => array(0 => 'WebMoney', 1 => 'Yandex Деньги', 2 => 'Robokassa'),
                    'htmlOptions' => array(
                        'style' => 'text-align: center',
                    ),
                ),
                array(
                    'name' => 'sum',
                    'htmlOptions' => array(
                        'style' => 'text-align: center',
                    ),
                ),
                array(
                    'name' => 'date',
                    'htmlOptions' => array(
                        'style' => 'text-align: center',
                    ),
                    'filter' => CHtml::textField("Pay[date]", $model->date),
                ),
            )
        )); ?>
    </div>
</div>
<script type="text/javascript">
    $(function () {
        $('#Pay_date').daterangepicker();
    });
</script>
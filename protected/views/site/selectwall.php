<?
Yii::app()->clientScript->registerScript('search',
    "var gid=$gid;
    $(\"select[name='select']\").on('change', function() {
        $.fn.yiiListView.update('list', {
            url: '" . CController::createUrl('site/SelectWall') . "',
            data: 'type='+$(\"select[name='select']\").val()+'&gid='+gid,
        }
    )}
    );"
);?>
<? $url = Yii::app()->getRequest()->getParam('url'); ?>

<div class="col-md-12">
    <h2>Записи со стены</h2>
    <hr/>
    <? $this->widget('zii.widgets.CBreadcrumbs', array(
        'htmlOptions' => array(
            'class' => 'breadcrumb'
        ),
        'homeLink' => '<a href="' . Yii::app()->getRequest()->getBaseUrl(true) . '">Главная</a>',
        'links' => array(
            'Альбомы группы' => array('/site/group', 'url' => $url),
            'Записи со стены'
        ),
    )); ?>
</div>
</div>
<form>
    <input type="hidden" name='gid' value='<?= $gid ?>'>
    <? if (Yii::app()->user->hasFlash('error')) { ?>
        <center><p><span style="font-size:20px"><strong>
                        <?= Yii::app()->user->getFlash('error'); ?>
                    </strong></span></p></center>
    <? } else { ?>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-md-2 offset">
                                <input name="textfield" type="text" class="form-control" placeholder="Введите текст поиска">
                            </div>
                            <div class="col-md-2 offset">
                                <button class="btn btn-primary form-control search" name="but" class="btn btn-primary btn-lg" value="1">Поиск</button>
                            </div>
                            <div class="col-md-2 offset">
                                <select name="select" class="form-control">
                                    <option value="0">Показать всё</option>
                                    <option value="1">Показать спам</option>
                                    <option value="2">Показать прочитнные</option>
                                    <option value="3">Показать отмеченные</option>
                                </select>
                            </div>
                            <div class="col-md-2 offset">
                                <select name="metka" class="form-control">
                                    <option value="metka-spam">Отметить как спам</option>
                                    <option value="metka-read">Отметить как прочитнные</option>
                                    <option value="metka-select">Отметить как отмеченные</option>
                                </select>
                            </div>
                            <div class="col-md-2 offset">
                                <button class="btn btn-primary form-control search" name="but" type="submit" class="btn btn-primary btn-lg" value="2">Отметить</button>
                            </div>
                            <div class="col-md-2 offset">
                                <button class="btn btn-primary form-control search" name="but" class="btn btn-primary btn-lg" value="3">Удалить</button>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body">
                        <? if (isset($dataProvider)) $this->renderPartial('list', array('dataProvider' => $dataProvider), false, true); ?>
                    </div>
                </div>
            </div>
        </div>
    <? } ?>
</form>


<div class="panel panel-default padding <?= $color[$data['tipeSelect']] ?>">
    <div class="row">
        <div class="col-md-2 col-xs-6">
            <input type="checkbox" name="news[]" class="album-checkbox" value="<?= $data['idWall'] ?>" id="<?= $data['idWall'] ?>">
            <? if (!empty($data['photoWall'])) { ?>
                <label for="<?= $data['idWall'] ?>"><img src="<?= $data['photoWall'] ?>" alt="..." width="200px"></label>
            <? } ?>
        </div>
        <div class="col-md-3 col-xs-6 col-md-offset-1">
            <label>Дата: </label> <?= $data['dataWall'] ?><br>
            <label>Пользователь:</label> <a href="https://vk.com/id<?= $data['idUser'] ?>"><?= $data['nameAlbum'] ?></a><br>
        </div>
        <div class="col-md-6 col-xs-12">
            <?= $data['textWall'] ?>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
<script type="text/javascript" src="<? Yii::app()->request->getBaseUrl(true) ?>/assets/admin/js/Date.js"></script>
<div class="col-md-12">
    <div class="panel panel-default">
        <div class="panel-heading">
            Аккаунт:&nbsp;<?= User::Data('name') ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <center><label class="status"></label></center>
            <br>
            <label class="countwall"></label><br>
            <label class="max"></label><br>
            <label class="nowCount"></label>
        </div>
        <div class="panel-body">
            <div class="tabs">
                <ul class="nav nav-pills nav-justified">
                    <li class="active"><a href="#tab-1" data-toggle="tab">Действия</a></li>
                    <li><a href="#tab-2" data-toggle="tab">Параметры</a></li>
                </ul>
            </div>
            <div class="tab-content">
                <div class="tab-pane fade in active" id="tab-1">
                    <div class="row">
                        <div class="col-md-12">
                            <button class="btn col-md-2 btn-offset col-xs-12 start">
                                <div class="image-size">
                                    <i class="fa fa-play-circle-o fa-x2"></i>
                                </div>
                                <div class="text-box">
                                    <p class="main-text">Старт</p>
                                </div>
                            </button>
                            <button class="btn col-md-2 btn-offset col-md-offset-1 col-xs-12 stop">
                                <div class="image-size" style="font-size:44px;margin-top:-2px;">
                                    <i class="fa fa-pause"></i>
                                </div>
                                <div class="text-box">
                                    <p class="main-text">Стоп</p>
                                </div>
                            </button>
                            <button class="btn col-md-2 btn-offset col-md-offset-1 col-xs-12 txt">
                                <div class="image-size">
                                    <i class="fa fa-share-square-o"></i>
                                </div>
                                <div class="text-box">
                                    <p class="main-text">в .txt</p>
                                </div>
                            </button>
                            <button class="btn col-md-2 btn-offset col-md-offset-1 col-xs-12 xls">
                                <div class="image-size">
                                    <i class="fa fa-share-square-o"></i>
                                </div>
                                <div class="text-box">
                                    <p class="main-text">в .xls</p>
                                </div>
                            </button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="export"></div>
                            <div class="progress progress-striped active load"></div>
                            <p class="loading text-center"></p>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="tab-2">
                    <?php $form = $this->beginWidget('CActiveForm', array(
                        'id' => 'user-form',
                        'enableAjaxValidation' => true,
                        'enableClientValidation' => true,
                        'action' => 'Site/Index',
                        'clientOptions' => array(
                            'validateOnSubmit' => true,
                            'validateOnChange' => true,
                        )
                    )); ?>
                    <? echo $form->errorSummary($model); ?>
                    <?php echo $form->hiddenField($model, 'idCity'); ?>
                    <?php echo $form->hiddenField($model, 'idUser', array('value' => Yii::app()->user->id)); ?>
                    <div class="alert hidden" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <?php echo $form->labelEx($model, 'idCountry'); ?>
                                <?php echo $form->dropDownList($model, 'idCountry', $country, array('class' => 'form-control')); ?>
                            </div>
                            <div class="form-group City">
                                <?php echo $form->labelEx($model, 'nameCity'); ?>
                                <?php echo $form->textField($model, 'nameCity', array('class' => 'form-control')); ?>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <?php echo $form->label($model, 'ageFrom'); ?><br>
                                <?php echo $form->textField($model, 'ageFrom', array('class' => 'form-control')); ?>
                                <?php echo $form->error($model, 'ageFrom'); ?>
                            </div>
                            <div class="form-group">
                                <?php echo $form->label($model, 'ageTo'); ?><br>
                                <?php echo $form->textField($model, 'ageTo', array('class' => 'form-control')); ?>
                                <?php echo $form->error($model, 'ageTo'); ?>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <?php echo $form->labelEx($model, 'dateFrom'); ?>
                                <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                                    'name' => 'dateFrom',
                                    'model' => $model,
                                    'attribute' => 'dateFrom',
                                    'language' => 'ru',
                                    'options' => array(
                                        'dateFormat' => 'yy-mm-dd',
                                        'showAnim' => 'fold',
                                    ),
                                    'htmlOptions' => array(
                                        'class' => 'form-control',
                                        'style' => 'height:34px;'
                                    ),
                                ));?>
                                <?php echo $form->error($model, 'dateFrom'); ?>
                            </div>
                            <div class="form-group">
                                <?php echo $form->labelEx($model, 'dateTo'); ?>
                                <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                                    'name' => 'dateTo',
                                    'model' => $model,
                                    'attribute' => 'dateTo',
                                    'language' => 'ru',
                                    'options' => array(
                                        'dateFormat' => 'yy-mm-dd',
                                        'class' => 'form-control',
                                        'showAnim' => 'fold',
                                    ),
                                    'htmlOptions' => array(
                                        'class' => 'form-control',
                                        'style' => 'height:34px;'
                                    ),
                                ));?>
                                <?php echo $form->error($model, 'dateTo'); ?>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group text-center radio-inline" style="padding: 30px 60px;">
                                <?php echo $form->radioButtonList($model, 'Sex', array(1 => 'жен', 2 => 'муж', 0 => 'любой')); ?>
                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <?php echo $form->labelEx($model, 'keyWord'); ?>
                                <?php echo $form->textArea($model, 'keyWord', array("class" => "form-control key", "rows" => "6")); ?>
                            </div>
                            <div class="form-group">
                                <?= CHtml::ajaxSubmitButton("Сохранить", 'Site/Index', array('success' =>
                                    'function(data){
                                        if (data=="Данные сохранены"){
                                             $("div[role=\"alert\"]").removeClass("alert-warning");
                                        $("div[role=\"alert\"]").removeClass("hidden");
                                        $("div[role=\"alert\"]").addClass("alert-success");
                                        $("div[role=\"alert\"]").html(data);
                                        } else {
                                                $("div[role=\"alert\"]").removeClass("hidden");
                                                $("div[role=\"alert\"]").addClass("alert-warning");
                                                $("div[role=\"alert\"]").html("Проверьте данные");
                                        }
                                    }
                                    '), array("class" => "btn btn-danger col-xs-12 col-md-4 col-md-offset-4")) ?>
                            </div>
                        </div>
                    </div>
                    <?php $this->endWidget(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-md-12">
    <? $this->widget('zii.widgets.CListView', array(
        'dataProvider' => $resul,
        'enablePagination' => true,
        'ajaxUpdate' => false,
        'itemView' => '_Wall',
        'summaryText' => 'Показаны записи {start} - {end} из {count}',
        'emptyText' => 'Нет результатов',
        'pager' => array(
            'class' => 'CLinkPager',
            'header' => '',
            'maxButtonCount' => 4,
            'htmlOptions' => array(
                'class' => 'pagination'
            ),
            'firstPageLabel' => 'Первая',
            'lastPageLabel' => 'Последняя',
            'cssFile' => Yii::app()->getRequest()->getBaseUrl(true) . '/assets/index/css/pager.css',
        ),
    ));?>
</div>


<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">

        <div class="modal-content">
            <p style="text-align: center;"><span style="font-size:26px"><strong>Добавить группу</strong></span></p><br>

            <div class="alert2" role="alert2"></div>
            <?php $form = $this->beginWidget('CActiveForm', array(
                'id' => 'group-form',
                'enableAjaxValidation' => true,
                'enableClientValidation' => true,
                'action' => 'Site/Index',
                'clientOptions' => array(
                    'validateOnSubmit' => true,
                    'validateOnChange' => true,
                )
            )); ?>
            <div class="form-group">
                <?php echo $form->labelEx($group, 'nameUrl'); ?>
                <?php echo $form->textField($group, 'nameUrl', array('class' => 'form-control')); ?>
                <?php echo $form->error($group, 'nameUrl'); ?>
            </div>
            <div class="form-group">
                <?php echo $form->labelEx($group, 'url'); ?>
                <?php echo $form->textField($group, 'url', array('class' => 'form-control')); ?>
                <?php echo $form->error($group, 'url'); ?>
            </div>
            <div class="row buttons">
                <?= CHtml::ajaxSubmitButton("Добавить", 'Site/groupadd', array('success' =>
                    'function(data){
                                if (data=="Данные сохранены"){
                                $("div[role=\"alert2\"]").removeClass("alert-warning");
                                $("div[role=\"alert2\"]").removeClass("hidden");
                                $("div[role=\"alert2\"]").addClass("alert-success");
                                $("div[role=\"alert2\"]").html(data);
                                location.reload();
                                } else {
                                        $("div[role=\"alert2\"]").removeClass("hidden");
                                        $("div[role=\"alert2\"]").addClass("alert-warning");
                                        $("div[role=\"alert2\"]").html("Проверьте данные");
                                }

                            }
                            '), array("class" => "btn btn-danger butongroup")) ?></div>
            <?php $this->endWidget(); ?>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        var correntCount = 0;
        var countWall;
        var max = '<?=$labelMax?>';
        var nowCount = '<?=$labelNow?>';
        var countAll = '<?=$countAll?>'
        var pid = '<?=$pid?>';
        if (pid == '') {
            $(".max").html('Найденные пользователи: ' + max);
            $(".countwall").html('Колличество постов: ' + countAll);
            $(".nowCount").html('Пройдено пользвателей: ' + nowCount);
            $('button.stop').addClass('darkgray');
            $('button.start').removeClass('darkgray');
            $('button.stop').attr('disabled', 'disabled');
            $('button.start').removeAttr('disabled', 'disabled');
        } else {
            $('button.stop').removeClass('darkgray');
            $('button.start').addClass('darkgray');
            $('button.stop').removeAttr('disabled', 'disabled');
            $('button.start').attr('disabled', 'disabled');
            countWall = setInterval(function () {
                $.ajax(
                        {
                            url: '/site/Values',
                            success: function (data) {
                                var arrayData = JSON.parse(data);
                                var correntCount;
                                if (max == 0) {
                                    $(".status").html('Идет поиск пользователей');
                                } else if (max != 0) {
                                    $(".status").html('Пользователи найдены, идет поиск записей');
                                }
                                if (arrayData['max'] > 0) {
                                    correntCount = arrayData['nowCount'] * 100 / arrayData['max'];
                                } else {
                                    correntCount = 0;
                                }
                                $(".loading").html(Math.round(correntCount) + '%');
                                $(".max").html('Найденные пользователи: ' + arrayData['max']);
                                $(".nowCount").html('Пройдено пользвателей: ' + arrayData['nowCount']);
                                $(".countwall").html('Колличество постов: ' + arrayData['now']);
                                $(".progress.progress-striped.active.load").html('<div class="progress-bar"  role="progressbar" aria-valuenow="' + correntCount + '" aria-valuemin="0" aria-valuemax="100" style="width: ' + correntCount + '%"><span class="sr-only">' + correntCount + '% Complete</span></div>');
                            }
                        })
            }, 2000)
        }
        $('button.txt').click(function () {
            $.ajax({
                url: '/site/Save',
                data: 'type=1',
                success: function (data) {
                    $(".export").html('<a target="_blank" href="export/' + data + '">Ссылка на скачивание .txt</a>');
                    }
                })
        })
        $('button.xls').click(function () {
            //$(".progress.progress-striped.active.load").html('');
            $.ajax({
                url: '/site/Save',
                data: 'type=2',
                success: function (data) {
                    $(".export").html('<a href="export/' + data + '">Ссылка на скачивание .xls</a>');
                    }
            })

        })
        $('button.start').click(function () {
            //$(".progress.progress-striped.active.load").html('');
            $.ajax({
                url: '/site/Posting',
                data: 'start=1'
            })
            //$(".loading").html('0%');
            $(".status").html('Поиск начат');
            $(".max").html('Найденные пользователи: 0');
            $(".nowCount").html('Пройдено пользвателей: 0');
            $(".countwall").html('Колличество постов: 0');
            $('button.stop').removeClass('darkgray');
            $('button.start').addClass('darkgray');
            $('button.stop').removeAttr('disabled', 'disabled');
            $('button.start').attr('disabled', 'disabled');

            countWall = setInterval(function () {
                $.ajax({
                    url: '/site/Values',

                    success: function (data) {
                        var arrayData = JSON.parse(data);
                        max = arrayData['max'];
                        if (max == 0) {
                            $(".status").html('Идет поиск пользователей');
                        } else if (max != 0) {
                            $(".status").html('Пользователи найдены, идет поиск записей');
                        }
                        if (arrayData['max'] > 0) {
                            correntCount = arrayData['nowCount'] * 100 / arrayData['max'];
                        } else {
                            correntCount = 0;
                        }
                        $(".loading").html(Math.round(correntCount) + '%');
                        $(".max").html('Найденные пользователи: ' + arrayData['max']);
                        $(".nowCount").html('Пройдено пользвателей: ' + arrayData['nowCount']);
                        $(".countwall").html('Колличество постов: ' + arrayData['now']);
                        $(".progress.progress-striped.active.load").html('<div class="progress-bar"  role="progressbar" aria-valuenow="' + correntCount + '" aria-valuemin="0" aria-valuemax="100" style="width: ' + correntCount + '%"><span class="sr-only">' + correntCount + '% Complete</span></div>');
                    }
                })
            }, 2000)
        })
        $('button.stop').click(function () {
            $.ajax(
                {
                    url: '/site/Posting',
                    data: 'start=0'
                })
            $(".status").html('Поиск остановлен');
            $('button.stop').addClass('darkgray');
            $('button.start').removeClass('darkgray');
            $('button.stop').attr('disabled', 'disabled');
            $('button.start').removeAttr('disabled', 'disabled');
            clearInterval(countWall);

            })
        $('input[name="Settings[nameCity]"]').change(function () {
            if (document.getElementById('Settings_nameCity').value.replace(/\s+/g, '').length) {
                $.ajax({
                    url: '/site/ajax',
                    data: 'act=getcity&countryId=' + $('select[name="Settings[idCountry]"]').val() + '&city=' + $('input[name="Settings[nameCity]"]').val(),
                    success: function (data) {
                        console.log(data);
                        if (data == 'bad') {
                            $('.City').removeClass("success");
                            $('.City').addClass("has-warning");
                            $('input[name="Settings[idCity]"]').val(0);
                        }
                        else if (data.match(/[\d]+$/)) {
                            $('.City').removeClass("has-warning");
                            $('.City').addClass("success");
                            $('input[name="Settings[idCity]"]').val(data);

                        }
                        }
                })
            }
        })
        })
    </script>



<div class="col-md-12 panel panel-default padding">
    <div class="col-md-2 col-xs-12">
        <input class="album-checkbox" type="checkbox" name="news[]" value="<?= $data['id'] ?>" id="<?= $data['id'] ?>">
        <label for="<?= $data['id'] ?>"><img src="<?= $data['image'] ?>" alt="..."></label>
    </div>
    <div class="col-md-9 col-xs-12 col-md-offset-1">
        <label>Альбом: </label> <a href="site/SelectGroup?aid=<?= $data['id'] ?>&url=<?= $url ?>"><?= $data['title'] ?></a><br>
        <label>Получено комментариев: </label> <span id="countComment<?= $data['id'] ?>"></span><br>
        <label>Ход выполнения</label>

        <div class="progress progress-striped active">
            <div class="progress-bar" role="progressbar" data-id="<?= $data['id'] ?>" aria-valuemin="0" aria-valuemax="100"></div>
        </div>
        <a class="btn btn-primary offset col-md-5" name="xls" target="_blank" href="site/exportAlbum?type=txt&id=<?= $data['id'] ?>">.xls</a>
        <a class="btn btn-primary offset col-md-5 col-md-offset-2" name="txt" target="_blank" href="site/exportAlbum?type=xls&id=<?= $data['id'] ?>">.txt</a>
    </div>
    <div class="clearfix"></div>
</div>
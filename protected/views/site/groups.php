<div class="col-md-12">
    <h2>Альбомы группы <?= $nameGroup ?></h2>
    <!--    <h5>В этом разделе вы можете выбрать удобный для Вас способ пополнения баланса</h5>-->
    <hr>
    <? $this->widget('zii.widgets.CBreadcrumbs', array(
        'htmlOptions' => array(
            'class' => 'breadcrumb'
        ),
        'homeLink' => '<a href="' . Yii::app()->getRequest()->getBaseUrl(true) . '">Главная</a>',
        'links' => array(
            'Альбомы группы ' . $nameGroup,
        ),
    )); ?>
</div>
</div>

<? if (Yii::app()->user->hasFlash('name')) {
    echo Yii::app()->user->getFlash('name');
} ?>

<? $form = $this->beginWidget('CActiveForm', array(
    'action' => 'site/group',
    'id' => 'group',
    'method' => 'post'
)) ?>
<div class="row">
    <div class="col-md-12">
        <?= CHtml::hiddenField('url', $url) ?>
        <div class="col-xs-12 col-md-6 panel panel-primary">
            <div class="col-md-12 form-group padding-top">
                <?= $form->checkBox($model, 'postImg') ?>
                <?= $form->labelEx($model, 'postImg') ?>
            </div>
            <div class="col-md-6 form-group">
                <label for="dateFrom">Дата с</label>
                <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'name' => 'DateWallFrom',
                    'model' => $model,
                    'attribute' => 'DateWallFrom',
                    'language' => 'ru',
                    'options' => array(
                        'dateFormat' => 'yy-mm-dd',
                        'showAnim' => 'fold',
                    ),
                    'htmlOptions' => array(
                        'class' => 'form-control',
                    ),
                ));?>
            </div>
            <div class="col-md-6 form-group">
                <label for="dateTo">Дата по</label>
                <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'name' => 'DateWallTo',
                    'attribute' => 'DateWallTo',
                    'model' => $model,
                    'language' => 'ru',
                    'options' => array(
                        'dateFormat' => 'yy-mm-dd',
                        'class' => 'form-control',
                        'showAnim' => 'fold',
                    ),
                    'htmlOptions' => array(
                        'class' => 'form-control',
                    ),
                )); ?>
            </div>
            <div class="col-md-6 form-group">
                <button class="search btn btn-primary form-control" name="but" type="submit" value="1">Старт</button>
            </div>
            <div class="col-md-6 form-group">
                <button class="searchstop btn btn-danger form-control" name="but" type="submit" value="2">Стоп</button>
            </div>
        </div>
        <div class="col-xs-12 col-md-5 col-md-offset-1 panel panel-primary">
            <table class="table table-condensed">
                <?= Wall::getStatistics($url) ?>
            </table>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="tabs">
                    <ul class="nav nav-pills nav-justified">
                        <li class="active"><a href="#tab-1" data-toggle="tab">Альбомы</a></li>
                        <li><a href="#tab-2" data-toggle="tab">Комментарии</a></li>
                    </ul>
                </div>
            </div>

            <div class="panel-body">
                <div class="tab-content">
                    <div class="tab-pane fade in active" id="tab-1">
                        <?
                        $all = 300;
                        $currentValue = (100 * $countwal) / $all;
                        $status = $currentValue > 0 ? $currentValue . ' / 100' : '0 / 100';
                        ?>
                        <h3>Стена</h3>

                        <div class="col-md-12 panel panel-default padding">
                            <div class="col-md-2 col-xs-5">
                                <input class="album-checkbox" type="checkbox" name="wall[]" value="<?= $url ?>" id="wall-check">
                                <label for="wall-check"><img src="/bvk2.png" width="100px" alt="..."></label>
                            </div>
                            <div class="col-md-9 col-xs-7 col-md-offset-1">
                                <label>Стена: </label> <a href="site/SelectWall?gid=<?= $idGroup ?>&url=<?= $url ?>">Стена</a><br>
                                <label>Получено комментариев: </label> <?= $countwal ?><br>
                                <label>Ход выполнения</label>

                                <div class="progress progress-striped active">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="<?= $currentValue ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?= $currentValue ?>%">
                                        <?= round($status, 1) ?>
                                    </div>
                                </div>
                                <a class="btn btn-primary offset col-md-5" name="xls" target="_blank" href="site/exportAlbum?type=walltxt&id=<?= $idGroup ?>">.xls</a>
                                <a class="btn btn-primary offset col-md-5 col-md-offset-2" name="txt" target="_blank" href="site/exportAlbum?type=wallxls&id=<?= $idGroup ?>">.txt</a>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <h3>Альбомы</h3>
                        <? $this->widget('zii.widgets.CListView', array(
                            'dataProvider' => $itemsProvider,
                            'viewData' => array(
                                'url' => $url,
                                'nameGroup' => $nameGroup
                            ),
                            'itemView' => '_group',
                            'summaryText' => 'Показаны альбомы {start} - {end} из {count}',
                            'emptyText' => 'Нет альбомов',
                            'template' => '{sorter}{items}{pager}',
                            'pager' => array(
                                'class' => 'CLinkPager',
                                'header' => '',
                                'maxButtonCount' => 4,
                                'htmlOptions' => array(
                                    'class' => 'pagination'
                                ),
                                'firstPageLabel' => 'Первая',
                                'lastPageLabel' => 'Последняя',
                                'cssFile' => Yii::app()->getRequest()->getBaseUrl(true) . '/assets/index/css/pager.css',
                            ),
                        ));?>
                    </div>
                    <div class="tab-pane fade" id="tab-2">
                        <? $this->widget('zii.widgets.CListView', array(
                            'dataProvider' => $Provider,
                            'summaryText' => 'Показаны комментарии {start} - {end} из {count}',
                            'itemView' => '_table',
                            'emptyText' => 'Комментариев нет',
                            'pager' => array(
                                'class' => 'CLinkPager',
                                'header' => '',
                                'maxButtonCount' => 4,
                                'htmlOptions' => array(
                                    'class' => 'pagination'
                                ),
                                'firstPageLabel' => 'Первая',
                                'lastPageLabel' => 'Последняя',
                                'cssFile' => Yii::app()->getRequest()->getBaseUrl(true) . '/assets/index/css/pager.css',
                            ),
                        ));?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<? $this->endWidget(); ?>
<script type="text/javascript">
    $(document).ready(function () {
        var countWall;
        var url = '<?$urlPage?>'
        var pid = '<?=$pidComment?>';
        if (pid == '') {
            $('button.searchstop').addClass('darkgray');
            $('button.search').removeClass('darkgray');
            $('button.searchstop').attr('disabled', 'disabled');
            $('button.search').removeAttr('disabled', 'disabled');
        } else {
            $('button.searchstop').removeClass('darkgray');
            $('button.search').addClass('darkgray');
            $('button.searchstop').removeAttr('disabled', 'disabled');
            $('button.search').attr('disabled', 'disabled');
        }
        $('button.search').click(function () {
            $.ajax({
                url: '/site/group',
                data: 'type=1'
            })
        })
        $('button.searchstop').click(function () {
            $.ajax({
                url: '/site/group',
                data: 'type=2',
                success: function (data) {

                }
            })
        })
        var stat = setInterval(function () {
            getStat();
        }, 5000);
    })

    function getStat() {
        var match = document.body.innerHTML.match(/value="([\d+]{2,})"/g),
            id = new Array();
        for (var i = 0; i < match.length; i++) {
            id.push(match[i].match(/\d+/)[0]);
        }
        $.ajax({
            url: 'site/stat',
            data: 'album_id=' + id,
            success: function (data) {
                var result = JSON.parse(data);
                for (var key in result) {
                    var val = result[key];
                    var current = Number(val['current']),
                        total = Number(val['total']),
                        comment = Number(val['comment']),
                        percent = Math.round(current * 100 / total);

                    if (current != 0 && total != 0) {
                        if (current == total) id
                        $('div[data-id=' + key + ']').attr('aria-valuenow', percent);
                        $('div[data-id=' + key + ']').css('width', percent + '%');
                        $('div[data-id=' + key + ']').html(percent + '%');
                        $('#countComment' + key).html(comment);
                    }
                }
            }
        })
    }
</script>

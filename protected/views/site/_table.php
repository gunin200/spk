<div class="col-md-12 panel panel-default padding">
    <div class="col-md-2 col-xs-6">
        <img src="<?= $data['photo'] ?>" alt="...">
    </div>
    <div class="col-md-3 col-xs-6 col-md-offset-1">
        <label>Альбом:</label> <?= $data['nameAlbum'] ?><br>
        <label>Дата:</label> <?= $data['date'] ?><br>
        <label>Автор:</label> <a href="https://vk.com/id<?= $data['user'] ?>"><?= $data['name'] ?></a>
    </div>
    <div class="col-md-6 col-xs-12">
        <?= $data['text'] ?>
    </div>
    <div class="clearfix"></div>
</div>
<!DOCTYPE html>
<html lang="ru">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="/assets/index/images/favicon.png?1">
    <base href="<?=Yii::app()->request->getBaseUrl(true)?>">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Сервис поиска клиентов SocSearch.info</title>
    <!-- normalizar -->
    <link href="assets/index/css/normalize.css" rel="stylesheet">

    <!-- Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Cabin:400,500,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>

    <!-- Bootstrap -->
    <link href="assets/index/css/bootstrap.min.css" rel="stylesheet">
    <!--fontawesome-->
    <link href="assets/index/css/font-awesome.min.css" rel="stylesheet">
    <!--animation-->
    <link href="assets/index/css/animate.min.css" rel="stylesheet">
    <!--main css-->
    <link href="assets/index/css/style.css" rel="stylesheet">
    <!--main css-->
    <link href="assets/index/css/responsive.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
    <div id="preloader">
        <i class="fa fa-spinner fa-pulse fa-3x fa-fw margin-bottom"></i>
    </div>
    
    <header id="header_area">
        <div class="container">
            <div class="row">
                <div class="col-sm-4">
                    <div class="logo">
                        <h2><a href="">ClientFinder</a></h2>
                    </div>
                </div>
                <div class="col-sm-8">
                    <div class="mainmenu">
                        <div class="navbar navbar-nobg">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>

                            <div class="navbar-collapse collapse">
                                <ul class="nav navbar-nav navbar-right">
                                    <li><a href="#">Главная</a>
                                    </li>
                                    <li><a href="#">О нас</a>
                                    </li>
                                    <li><a href="https://oauth.vk.com/authorize?client_id=<?=Yii::app()->params['vk_application_id']?>&scope=photos,groups,offline&redirect_uri=<?=Yii::app()->request->getBaseUrl(true)?>/site/login&response_type=code&v=5.33"><div class="fa fa-sign-in"></div> Вход</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="header_area_text">
                        <h2 class="wow slideInDown" data-wow-duration="2s">Сервис поиска клиентов на товары и услуги из "ВКонтакте"</h2>
                        <p class="wow slideInUp">Наш сервис осуществляет поиск клиентов посредством поиска записей на стене и поиска комментариев к фотографиям в альбомах групп.</p>
                        <a class="wow slideInUp" data-wow-duration="2s" href="">Попробуй уже сейчас</a>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!-- end header top area -->


    <section id="features_area" class="section_padding text-center">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h2>Наши возможности</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <div class="single_feature wow slideInUp" data-wow-duration="1s">
                        <i class="fa fa-search"></i>
                        <h3>Поиск постов</h3>
                        <p>Достаточно указать ключевые слова, например - "Сниму квартиру", указать критерии поиска и сервис найдет сообщения людей, которые хотят снять квартиру. Множество критериев поиска: возраст, город, пол, дата поста и ключевые слова.</p>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="single_feature wow slideInUp" data-wow-duration="2s">
                        <i class="fa fa-comments"></i>
                        <h3>Поиск комментариев</h3>
                        <p>Достаточно указать группу конкурента и наш сервис соберет все комментарии со стены и из фотоальбомов. Среди них вы легко найдете своих клиентов.</p>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="single_feature wow slideInUp" data-wow-duration="3s">
                        <i class="fa fa-sort-amount-asc"></i>
                        <h3>Дополнительно</h3>
                        <p>Удобный интерфейс позволяет быстро и легко перехватить клиентов из собранных комментариев. Все результаты поиска можно сохранить в удобном формате таблицы Excel</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- end features area -->

    <section id="story_area" class="section_padding">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="story_image wow slideInLeft" data-wow-duration="2s">
                        <img src="assets/index/images/bg.jpg" alt="">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="story_text wow slideInRight" data-wow-duration="2s">
                        <h2>Вам нужен наш сервис если...</h2>
                        <p>Вы риелтор и ищите людей, которые хотят сдать, снять, продать или купить квартиру. В социальных сетях часто ищут подобные услуги.</p>
                        <p>Вы фотограф, юрист, строитель, владелец интернет-магазина, покупатель/продавец автомобилей и любой человек, которому нужно найти клиентов.</p>
                        <p>Вы желаете выйти на рынок, но конкуренция высока. Наш сервис позволит переманить клиентов из групп соперников.</p>
                        <a href=""><div class="fa fa-vk"></div> Войти</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- end of story area -->

    <section id="clients_say_area" class="section_padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h2>Примеры постов</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="clients_say wow slideInUp" style="visibility: hidden; -webkit-animation-name: none;">
                        <p>Сдаем посуточно однушку
                        В отличном состоянии, 1200 рублец</p>
                        <div class="clients_say_list fix">
                            <div class="say floatleft">
                                <h5>Ооо Консул</h5>
                                <h6><a href="https://vk.com/wall227293234_2229">https://vk.com/wall227293234_2229</a></h6>
                            </div>
                            <div class="c_img floatright">
                                <img src="assets/index/images/client.png" alt="">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="clients_say wow slideInUp" style="visibility: hidden; -webkit-animation-name: none;">
                        <p>Сдаю отличную, современную 2к квартиру на ул.50 лет Победы д.11( ст. Варя). 4 этаж 4х этажного дома (сталинка). Фотографии реальные. Цена 15000.</p>
                        <div class="clients_say_list fix">
                            <div class="say floatleft">
                                <h5>Ольга Евсеева-мирохина</h5>
                                <h6><a href="https://vk.com/wall133137875_168">https://vk.com/wall133137875_168</a></h6>
                            </div>
                            <div class="c_img floatright">
                                <img src="assets/index/images/client.png" alt="">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="clients_say wow slideInUp" style="visibility: hidden; -webkit-animation-name: none;">
                        <p>Сдам квартиру на ул.Лебедева за 8000 руб. в мес., 33 кв.м, есть мебель ,бытовая техника,ремонт косметический,свежий, улучшенная планировка,с/у совмещен,душевая кабина,этаж 3, есть балкон,при необходимости можно провести интернет и каб.тв.</p>
                        <div class="clients_say_list fix">
                            <div class="say floatleft">
                                <h5>*СРОЧНЫЕ ОБЪЯВЛЕНИЯ * УЛАН-УДЭ*БУРЯТИЯ*</h5>
                                <h6><a href="https://vk.com/wall-62371533_15648">https://vk.com/wall-62371533_15648</a></h6>
                            </div>
                            <div class="c_img floatright">
                                <img src="assets/index/images/client.png" alt="">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="clients_say wow slideInUp" style="visibility: hidden; -webkit-animation-name: none;">
                        <p>Продам
Фотоапарат Зеніт Е з об'єктивом Геліос 44-2та блендою .У доброму стані. Лінзи та механіка у відмінному стані. Об'єктив може бути використаний на сучасних дзеркальних камерах через перехідник. На будь-які питання відповім.</p>
                        <div class="clients_say_list fix">
                            <div class="say floatleft">
                                <h5>Киевская барахолка</h5>
                                <h6><a href="https://vk.com/wall-20911479_22705">https://vk.com/wall-20911479_22705</a></h6>
                            </div>
                            <div class="c_img floatright">
                                <img src="assets/index/images/client.png" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="caal_to_action_area">
        <div class="container">
            <div class="row">
                <div class="col-sm-8">
                    <div class="call_to_action_text wow slideInLeft" data-wow-duration="2s">
                        <h2>Готовы начать?</h2>
                        <p>Просто зайдите на сайт через свой аккаунт ВКонтакте</p>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="call_project text-right wow slideInRight" data-wow-duration="2s">
                        <a href=""><div class="fa fa-vk"></div> Войти</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- end call to action area -->


    <footer id="footer_area">
        <div class="container">
            <div class="row">
                <div class="col-sm-3">
                    <div class="company_logo wow slideInDown">
                        <h2>ClientFinder</h2>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="company_address wow slideInDown">
                        <h2>E-Mail</h2>
                        <h3>example@gmail.com</h3>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="company_address wow slideInDown">
                        <h2>Skype</h2>
                        <h3>skype_exam</h3>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="company_address wow slideInDown">
                        <h2>ICQ</h2>
                        <h3>777 777</h3>
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <!-- main jQuery-->
    <script src="assets/index/js/jquery-1.11.3.min.js"></script>

    <!-- bootstrap js -->
    <script src="assets/index/js/bootstrap.min.js"></script>

    <!-- wow js -->
    <script src="assets/index/js/wow.min.js"></script>
    <script>
        new WOW().init();
    </script>

    <!-- main js -->
    <script src="assets/index/js/main.js"></script>

</body>

</html>
<div class="col-md-12">
    <h2>Пополнение баланса</h2>
    <h5>В этом разделе вы можете выбрать удобный для Вас способ пополнения баланса</h5>
    <hr>
    <? $this->widget('zii.widgets.CBreadcrumbs', array(
        'htmlOptions' => array(
            'class' => 'breadcrumb'
        ),
        'homeLink' => '<a href="' . Yii::app()->getRequest()->getBaseUrl(true) . '">Главная</a>',
        'links' => array(
            'Пополнение баланса'
        ),
    )); ?>
    <? $flash = Yii::app()->user->getFlashes(); ?>
    <? if ($flash): ?>
        <? foreach ($flash as $key => $value) : ?>
            <div class="alert alert-<?= $key ?>" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <?= $value ?>
            </div>
        <? endforeach; ?>
    <? endif; ?>
</div>
<div class="col-md-6 col-md-offset-3">
    <?= CHtml::beginForm('', 'get', array('class' => 'text-center')); ?>
    <div class="form-group">
        <?= CHtml::activeRadioButtonList($model, 'type_pay',
            array(
                0 => 'WebMoney',
                1 => 'Yandex Деньги',
                2 => 'Robokassa',
            )
        ) ?>
        </div>
    <div class="form-group">
        <?= CHtml::activeLabel($model, 'sum') ?>
        <?= CHtml::activeTextField($model, 'sum', array('class' => 'form-control')) ?>
    </div>
    <div class="form-group">
        <?= CHtml::submitButton('Оплатить', array('class' => 'btn btn-primary form-control')) ?>
    </div>
    <?= CHtml::endForm(); ?>
</div>
<div class="col-md-12">
    <h2>Купить подписку</h2>
    <h5>Приобретите подписку, чтобы пользоваться услугами сервиса</h5>
    <hr>
</div>

<div class="col-md-3 col-md-offset-2">
    <div class="panel panel-primary text-center no-boder bg-color-orange">
        <div class="panel-body">
            <i class="fa fa-battery-quarter fa-5x"></i>
            <h4>Тестовый период*</h4>
            <h4>20 <i class="fa fa-rub"></i></h4>
        </div>
        <div class="panel-footer back-footer-orange">
            <a type="button" class="btn btn-default btn-circle btn-pay" href="/site/buysubs?type=1">
                <i class="fa fa-check"></i>
            </a>
        </div>
    </div>
</div>
<div class="col-md-2 text-center">
    <h1>ИЛИ</h1>
</div>
<div class="col-md-3">
    <div class="panel panel-primary text-center no-boder bg-color-green">
        <div class="panel-body">
            <i class="fa fa-battery-full fa-5x"></i>
            <h4>Подписка на 1 месяц</h4>
            <h4>500 <i class="fa fa-rub"></i></h4>
        </div>
        <div class="panel-footer back-footer-green">
            <a type="button" class="btn btn-default btn-circle btn-pay" href="/site/buysubs?type=2">
                <i class="fa fa-check"></i>
            </a>
        </div>
    </div>
</div>
<div class="col-md-12">
    <i>* Тестовый период предоставляется всего лишь 1 раз</i>
</div>
<?php

class SiteController extends Controller
{
    function init()
    {
        $this->layout = 'main';
    }

    public function actionBuySubs()
    {
        if (!Yii::app()->user->isGuest) {
            $type = Yii::app()->getRequest()->getParam('type');
            if ($type != '') {
                $price = Price::model()->find();

                $user = User::model()->findByPk(Yii::app()->user->id);
                $datePay = $user->datePay;
                if ($datePay < date('Y-m-d H:i:s')) $datePay = date('Y-m-d H:i:s');

                $pay = new Pay;
                $pay->date = date('Y-m-d H:i:s');
                $pay->type = 0;
                $pay->type_pay = 5;
                $pay->user_id = Yii::app()->user->id;

                if ($type == 1) {
                    if ($user->userBalance < $price->testPrice) {
                        Yii::app()->user->setFlash('danger', 'На Вашем балансе недостаточно денежных средств!');
                        $this->redirect(Yii::app()->getRequest()->getBaseUrl(true) . '/payment');
                    }

                    if ($user->test) {
                        Yii::app()->user->setFlash('danger', 'Тестовый период недоступен!');
                        $this->redirect(Yii::app()->getRequest()->getBaseUrl(true) . '/payment');
                    }

                    $pay->sum = $price->testPrice;

                    $user->datePay = date('Y-m-d H:i:s', strtotime($datePay . ' +1 day'));
                    $user->typeSubs = 1;
                    $user->test = 1;
                    $user->userBalance -= $price->testPrice;

                    if ($user->save() && $pay->save()) {
                        Yii::app()->user->setFlash('success', 'Тестовый период успешно активирован!');
                        $this->redirect(Yii::app()->getRequest()->getBaseUrl(true) . '/payment');
                    }
                } elseif ($type == 2) {
                    if ($user->userBalance < $price->mounthPrice) {
                        Yii::app()->user->setFlash('danger', 'На Вашем балансе недостаточно денежных средств!');
                        $this->redirect(Yii::app()->getRequest()->getBaseUrl(true) . '/payment');
                    }

                    $pay->sum = $price->mounthPrice;

                    $user->datePay = date('Y-m-d H:i:s', strtotime($datePay . ' +31 day'));
                    $user->typeSubs = 1;
                    $user->test = 1;
                    $user->userBalance -= $price->mounthPrice;

                    if ($user->save() && $pay->save()) {
                        Yii::app()->user->setFlash('success', 'Подписка на 1 месяц успешно активирована!');
                        $this->redirect(Yii::app()->getRequest()->getBaseUrl(true) . '/payment');
                    }
                }
            }
        } else $this->redirect(Yii::app()->getRequest()->getBaseUrl(true));
    }

    public function actionStat()
    {
        $id = Yii::app()->getRequest()->getParam('album_id');
        $id = explode(',', $id);
        $settings = Settings::model()->find('idUser = :id', array(':id' => Yii::app()->user->id));
        $criteria = new CDbCriteria;
        $criteria->select = 'totalPhoto, COUNT(*) AS currentPhoto';
        $criteria->condition = 'idSettings = :id AND idAlbum = :aid';
        $criteria->group = 'photoWall';
        $result = array();
        for ($i = 0; $i < count($id); $i++) {
            $criteria->params = array(':id' => $settings->idSettings, ':aid' => $id[$i]);
            $wall = Wall::model()->findAll($criteria);
            $result[$id[$i]]['total'] = 0;
            $result[$id[$i]]['current'] = 0;
            $result[$id[$i]]['comment'] = 0;
            if (count($wall)) {
                if ($wall[0]->totalPhoto != null)
                    $result[$id[$i]]['total'] = (int)$wall[0]->totalPhoto;
                $result[$id[$i]]['current'] = (int)count($wall);
                foreach ($wall as $key => $value) {
                    $result[$id[$i]]['comment'] += $value['currentPhoto'];
                }
            }
        }
        $result = json_encode($result);
        echo $result;
    }

    public function actionIndex()
    {
        if (Yii::app()->user->isGuest) {
            $this->layout = 'index';
            $this->render('index');
        } else {

            if (Yii::app()->request->isAjaxRequest) {

                $model = Settings::model()->find('idUser=:myParams', array(':myParams' => Yii::app()->user->id));
                if (Count($model) == 1) {

                    $this->performAjaxValidation($model, 'user-form');
                    if (isset($_POST['Settings'])) {
                        $model->attributes = $_POST['Settings'];
                        if ($model->nameCity == '') {
                            $model->idCity = NULL;
                        }
                        //die (var_dump($model));
                        if ($model->save()) {
                            echo('Данные сохранены');
                        }

                    }

                } else {
                    $model = new Settings;

                    $this->performAjaxValidation($model, 'user-form');
                    if (isset($_POST['Settings'])) {
                        $model->attributes = $_POST['Settings'];
                        if ($model->nameCity == '') {
                            $model->idCity = 0;
                        }

                        if ($model->save()) {
                            echo('Данные сохранены');
                        }
                    }
                }
            } else {

                $model = Settings::model()->find('idUser=:myParams', array(':myParams' => Yii::app()->user->id));
                if (Count($model) == 0) {
                    $model = new Settings;
                } else {
                    if ($model->dateFrom == '0000-00-00') {
                        $model->dateFrom = '';
                    }
                    if ($model->dateTo == '0000-00-00') {
                        $model->dateTo = '';
                    }
                    //die(var_dump($model));
                    if ($model->idCity != 0) {
                        $response = SendHelper::send('https://api.vk.com/method/database.getCitiesById?lang=ru&city_ids=' . $model->idCity);
                        preg_match('/name":"(.*?)"}/', $response, $matches);
                        $model->nameCity = $matches[1];
                    } else {
                        $model->nameCity = '';
                    }
                }
                $response = SendHelper::send('https://api.vk.com/method/database.getCountries?lang=ru&need_all=1&count=234', "", "", "", true);
                preg_match_all('/"cid":(.*?),"title":"(.*?)"/', $response, $matches);
                $country = array_combine($matches[1], $matches[2]);
                ///$resul = Wall::model()->findAll();
                $dataProvider = new CActiveDataProvider('Wall', array(
                    'criteria' => array(
                        'condition' => 't.tipeText=0',
                        'order' => 'idWall DESC',
                        'with' => array(
                            'Settings' => array(
                                'condition' => 'Settings.idUser=:id',
                                'params' => array(
                                    ':id' => Yii::app()->user->id))))));
                $response = Settings::model()->find('idUser = :id', array(':id' => Yii::app()->user->id));
                if (count($response) > 0) {
                    $pid = $response->Pid;
                    $labelMax = $response->countWall;
                    $labelNow = $response->nowCount;
                } else {
                    $pid = '';
                    $labelMax = '0';
                    $labelNow = '0';
                }
                $res = Wall::model()->with(array('Settings' => array('condition' => 'Settings.idUser=:id', 'params' => array(':id' => Yii::app()->user->id))))->findAll('tipeText=0');

                $countAll = count($res);

                $urlTxt = $response->nameTxt;
                $urlXls = $response->nameXls;
                $groupRes = Wall::model()->with(array('Settings' => array('condition' => 'Settings.idUser=:id', 'params' => array(':id' => Yii::app()->user->id))))->findAll('tipeText=0');


                $group = new UserGroup;

                $this->render('cabinet', array('country' => $country, 'countAll' => $countAll, 'groupRes' => $groupRes, 'labelMax' => $labelMax, 'labelNow' => $labelNow, 'urlXls' => $urlXls, 'urlTxt' => $urlTxt, 'group' => $group, 'pid' => $pid, 'model' => $model, 'resul' => $dataProvider));


            }
        }
        $pars = Yii::app()->getRequest()->getParam('pars');

    }

    protected function performAjaxValidation($model, $form)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === $form) {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionLogin()
    {

        $code = Yii::app()->request->getParam('code');
        $response = SendHelper::send('https://oauth.vk.com/access_token?client_id=' .
            Yii::app()->params['vk_application_id'] . '&client_secret=' . Yii::app()->params['vk_client_secret'] . '&code=' . $code . '&redirect_uri=' .
            Yii::app()->request->getBaseUrl(true) . '/site/login', '', '', '', true);
        $response = json_decode($response, 1);
        if (isset($response['access_token'])) {
            $token = $response['access_token'];
            $id = $response['user_id'];
            Yii::app()->session['urlPhoto'] = User::avatar($id);

            if ($token != '') {

                $response = SendHelper::send('https://api.vk.com/method/users.get?user_ids=' . $id, '', '', '', true);
                $response = json_decode($response, 1);

                if ($response['response'][0]['first_name']) {

                    $user = new User;
                    $user->userFirstName = $response['response'][0]['first_name'];
                    $user->userLastName = $response['response'][0]['last_name'];
                    $user->token = $token;
                    $user->idUser = $id;
                    $user->role = 'Пользователи';

                    $count = User::model()->count('idUser = :id', array(':id' => $id));

                    if ($count == 0) {
                        $user->save();

                        $settings = new Settings;
                        $settings->idUser = $id;
                        $settings->save(false);
                    }

                    $identity = new UserIdentity('', '');
                    $identity->id = $id;
                    $identity->setState('id', $id);
                    Yii::app()->user->login($identity);

                    $this->redirect(Yii::app()->request->getBaseUrl(true));
                }
            }

        } else $this->redirect(Yii::app()->request->getBaseUrl(true));

    }

    public function actionLogout()
    {
        Yii::app()->user->logout();
        $this->redirect(Yii::app()->request->getBaseUrl(true));
    }

    public function actionAjax()
    {
        $act = Yii::app()->request->getParam('act');
        if ($act == 'getcity') {

            $countryId = Yii::app()->getRequest()->getParam('countryId');
            $city = urlencode(Yii::app()->getRequest()->getParam('city'));
            $response = SendHelper::send('https://api.vk.com/method/database.getCities?country_id=' . $countryId . '&region_id=&q=' . $city . '&need_all=&offset=0&count=1&version=5.34');
            preg_match('/"cid":(\d+),/', $response, $matches);
            if (isset ($matches[1])) {
                echo $matches[1];
            } else {
                echo 'bad';
            }
        }
    }

    public function actionPosting()
    {


        if (Yii::app()->request->isAjaxRequest) {
            $status = Yii::app()->getRequest()->getParam('start');
            if ($status == 1) {
                $response = Wall::model()->with(array('Settings' => array('condition' => 'Settings.idUser=:id', 'params' => array(':id' => Yii::app()->user->id))))->deleteAll('tipeText=0');
                $response = Settings::model()->find('idUser = :id', array(':id' => Yii::app()->user->id));
                $response->countWall = 0;
                $response->nowCount = 0;
                $response->save();
                //die('sdfsdgszdg'.$status);
                $command = "/usr/local/php/bin/php -f " . Yii::app()->params['path'] . "/protected/cron.php Posting " . Yii::app()->user->id;

                ///die ($command);
                $process = new Process($command);

                $pid = $process->getPid();
                $response = Settings::model()->find('idUser = :id', array(':id' => Yii::app()->user->id));
                $response->Pid = $pid;
                $response->save();
            } else {

                $response = Settings::model()->find('idUser = :id', array(':id' => Yii::app()->user->id));

                $process = new Process;
                $process->countWall = '0';
                $process->nowCount = '0';
                $process->setPid($response->Pid);
                $process->stop();
                $response->Pid = NULL;
                $response->save();
            }
        }
    }

    public function actionValues()
    {
        $response = Wall::model()->with(array('Settings' => array('condition' => 'Settings.idUser=:id', 'params' => array(':id' => Yii::app()->user->id))))->findAll('tipeText=0');
        $res = Settings::model()->find('idUser = :id', array(':id' => Yii::app()->user->id));
        if (count($res) > 0) {
            $result['max'] = (int)$res->countWall;
            $result['now'] = count($response);
            $result['nowCount'] = (int)$res->nowCount;
        } else {
            $result['max'] = 0;
            $result['now'] = 0;
            $result['nowCount'] = 0;
        }

        $decode = json_encode($result);
        echo($decode);
    }

    public function actionSave()
    {
        if (Yii::app()->request->isAjaxRequest) {
            $type = Yii::app()->getRequest()->getParam('type');
            if ($type == 1) {
                $response = Wall::model()->with(array('Settings' => array('condition' => 'Settings.idUser=:id', 'params' => array(':id' => Yii::app()->user->id))))->findAll('tipeText=0');

                $res = User::model()->findAll('idUser = :id', array(':id' => Yii::app()->user->id));
                $model = Settings::model()->find('idUser = :id', array(':id' => Yii::app()->user->id));
                $model->nameTxt = NULL;
                if ($model->nameTxt == NULL) {
                    $log = '';
                    for ($i = 0; $i < count($response); $i++) {
                        $log .= 'Дата поста:' . $response[$i]->dataWall . "\r\n";
                        $log .= 'Ссылка пользователя: https://vk.com/id' . $response[$i]->idUser . "\r\n";
                        $log .= 'Фото поста:' . $response[$i]->photoWall . "\r\n";
                        $log .= 'Текст поста:' . $response[$i]->textWall . "\r\n";
                        $log .= "\r\n" . '========================================================================' . "\r\n\r\n";
                    }
                    file_put_contents('export/' . $res[0]->userFirstName . '_' . $res[0]->userLastName . '_' . date('Y-m-d H:i:s') . '.txt', $log);
                    $model->nameTxt = $res[0]->userFirstName . '_' . $res[0]->userLastName . '_' . date('Y-m-d H:i:s') . '.txt';
                    $model->dateTxt = date('Y-m-d H:i:s');
                    $model->save();
                    echo $model->nameTxt;
                } elseif ($model->nameTxt != NULL) {
                    echo $model->nameTxt;
                }

            } elseif ($type == 2) {

                $xls = '';
                $response = Wall::model()->with(array('Settings' => array('condition' => 'Settings.idUser=:id', 'params' => array(':id' => Yii::app()->user->id))))->findAll('tipeText=0');
                $model = Settings::model()->find('idUser = :id', array(':id' => Yii::app()->user->id));
                $res = User::model()->findAll('idUser = :id', array(':id' => Yii::app()->user->id));
                $model->nameXls = NULL;
                if ($model->nameXls == NULL) {
                    for ($i = 0; $i < count($response); $i++) {
                        $xls .= 'Дата поста:' . $response[$i]->dataWall . "\n";
                        $xls .= 'Ссылка пользователя: https://vk.com/id' . $response[$i]->idUser . "\n";
                        $xls .= 'Фото поста:' . $response[$i]->photoWall . "\n";
                        $xls .= 'Текст поста:' . $response[$i]->textWall . "\n\n";
                    }
                    $xls = iconv("UTF-8", "CP1251//IGNORE", $xls);
                    file_put_contents('export/' . $res[0]->userFirstName . '_' . $res[0]->userLastName . '_' . date('Y-m-d H:i:s') . '.csv', $xls);
                    $model->dateXls = date('Y-m-d H:i:s');
                    $model->nameXls = $res[0]->userFirstName . '_' . $res[0]->userLastName . '_' . date('Y-m-d H:i:s') . '.csv';
                    $model->save();
                    echo $model->nameXls;
                } else {
                    echo $model->nameXls;
                }
            }
        }
        $command = "/usr/local/php/bin/php -f " . Yii::app()->params['path'] . "/protected/cron.php Export " . Yii::app()->user->id;
        $process = new Process($command);


    }

    public function actionSelectWall()
    {
        $gid = Yii::app()->getRequest()->getParam('gid');
        if (Yii::app()->request->isAjaxRequest) {
            $select = Yii::app()->getRequest()->getParam('type');
            if (empty($select))
                $select = 0;
            if ($select <= 3 && $select >= 0) {
                $dataProvider = new CActiveDataProvider('Wall', array(
                    'criteria' => array(
                        'order' => 't.IdWall desc',
                        'params' => array(':id' => Yii::app()->user->id, ':type' => $select, ':idalbum' => $gid),
                        'with' => array(
                            'Settings' => array(
                                'condition' => 'Settings.idUser=:id and tipeText=3 and  t.tipeSelect = :type and t.id_group= :idalbum',
                            ),
                        ),
                    ),
                    'pagination' => array(
                        'pageSize' => 15
                    ),
                ));
                $this->renderPartial('list', array('dataProvider' => $dataProvider, 'gid' => $gid), false, true);
            }
        } else {
            if (Yii::app()->getRequest()->getParam('but') == '2' && Yii::app()->getRequest()->getParam('metka') == 'metka-spam') {
                $news = Yii::app()->getRequest()->getParam('news');
                $gid = Yii::app()->getRequest()->getParam('gid');
                $edit = Yii::app()->getRequest()->getParam('textfield');
                for ($x = 0; $x < count($news); $x++) {
                    $respon = Wall::model()->with(array('Settings' => array('condition' => 'Settings.idUser=:id', 'params' => array(':id' => Yii::app()->user->id))))->find('id_group=' . $gid . ' and tipeText=3 and idWall=' . $news[$x]);
                    $respon->tipeSelect = '1';
                    $respon->save();
                }
                $nameAlbum = $respon->nameUser;
                // $this->render('selectgroup', array('aid' => $aid,'nameAlbum'=>$nameAlbum));
                $this->redirect(Yii::app()->request->getBaseUrl(true) . '/site/SelectWall?gid=' . $gid);
            }

            if (Yii::app()->getRequest()->getParam('but') == '2' && Yii::app()->getRequest()->getParam('metka') == 'metka-read') {

                $news = Yii::app()->getRequest()->getParam('news');
                $gid = Yii::app()->getRequest()->getParam('gid');
                $edit = Yii::app()->getRequest()->getParam('textfield');

                for ($x = 0; $x < count($news); $x++) {
                    $respon = Wall::model()->with(array('Settings' => array('condition' => 'Settings.idUser=:id', 'params' => array(':id' => Yii::app()->user->id))))->find('id_group=' . $gid . ' and tipeText=3 and idWall=' . $news[$x]);
                    $respon->tipeSelect = '2';
                    $respon->save();
                }
                $nameAlbum = $respon->nameUser;
                // $this->render('selectgroup', array('aid' => $aid,'nameAlbum'=>$nameAlbum,));
                $this->redirect(Yii::app()->request->getBaseUrl(true) . '/site/SelectWall?gid=' . $gid);
            }

            if (Yii::app()->getRequest()->getParam('but') == '2' && Yii::app()->getRequest()->getParam('metka') == 'metka-select') {

                $news = Yii::app()->getRequest()->getParam('news');
                $gid = Yii::app()->getRequest()->getParam('gid');
                $edit = Yii::app()->getRequest()->getParam('textfield');

                for ($x = 0; $x < count($news); $x++) {
                    $respon = Wall::model()->with(array('Settings' => array('condition' => 'Settings.idUser=:id', 'params' => array(':id' => Yii::app()->user->id))))->find('id_group=' . $gid . ' and tipeText=3 and idWall=' . $news[$x]);
                    $respon->tipeSelect = '3';
                    $respon->save();
                }
                $nameAlbum = $respon->nameUser;
                //$this->render('selectgroup', array('aid' => $aid,'nameAlbum'=>$nameAlbum));
                $this->redirect(Yii::app()->request->getBaseUrl(true) . '/site/SelectWall?gid=' . $gid);
            }

            if (Yii::app()->getRequest()->getParam('but') == '3') {
                $news = Yii::app()->getRequest()->getParam('news');
                $gid = Yii::app()->getRequest()->getParam('gid');
                $edit = Yii::app()->getRequest()->getParam('textfield');
                $res = Wall::model()->with(array('Settings' => array('condition' => 'Settings.idUser=:id', 'params' => array(':id' => Yii::app()->user->id))))->find('id_group=' . $gid);

                for ($x = 0; $x < count($news); $x++) {
                    $respon = Wall::model()->with(array('Settings' => array('condition' => 'Settings.idUser=:id', 'params' => array(':id' => Yii::app()->user->id))))->deleteAll('id_group=' . $gid . 'and tipeText=3 and idWall=' . $news[$x]);

                }

                $nameAlbum = $res->nameUser;

                // $this->render('selectgroup', array('aid' => $aid,'nameAlbum'=>$nameAlbum));
                $this->redirect(Yii::app()->request->getBaseUrl(true) . '/site/SelectWall?gid=' . $gid);
            }

            if (Yii::app()->getRequest()->getParam('but') == '1') {
                $edit = Yii::app()->getRequest()->getParam('textfield');
                $gid = Yii::app()->getRequest()->getParam('gid');
                $respon = Wall::model()->with(array('Settings' => array('condition' => 'Settings.idUser=:id', 'params' => array(':id' => Yii::app()->user->id))))->find('id_group=' . $gid);
                $nameAlbum = $respon->nameUser;
                $dataProvider = new CActiveDataProvider('Wall', array(
                    'criteria' => array(
                        'order' => 't.IdWall desc',
                        'condition' => 't.textWall LIKE :word',
                        'params' => array(':word' => '%' . $edit . '%'),
                        'with' => array(
                            'Settings' => array(
                                'condition' => 'Settings.idUser=:id and tipeText=3 and t.id_group= :idalbum',
                                'params' => array(':id' => Yii::app()->user->id, ':idalbum' => $gid),
                            ),
                        ),
                    ),
                    'pagination' => array(
                        'pageSize' => 15
                    ),
                ));
                $this->render('selectgroup', array('dataProvider' => $dataProvider, 'nameAlbum' => $nameAlbum, 'gid' => $gid));
            } else {
                $gid = Yii::app()->getRequest()->getParam('gid');
                $select = Yii::app()->getRequest()->getParam('type');
                $respon = Wall::model()->with(array('Settings' => array('condition' => 'Settings.idUser=:id', 'params' => array(':id' => Yii::app()->user->id))))->find('id_group=' . $gid);
                if (count($respon) > 0) {
                    $nameAlbum = $respon->nameUser;

                    $dataProvider = new CActiveDataProvider('Wall', array(
                        'criteria' => array(
                            'order' => 't.IdWall desc',
                            'params' => array(':id' => Yii::app()->user->id, ':idalbum' => $gid),
                            'with' => array(
                                'Settings' => array(
                                    'condition' => 'Settings.idUser=:id and tipeText=3 and t.id_group= :idalbum and t.tipeSelect = 0',
                                ),
                            ),
                        ),
                        'pagination' => array(
                            'pageSize' => 15
                        ),
                    ));
                    $this->render('selectwall', array('dataProvider' => $dataProvider, 'nameAlbum' => $nameAlbum, 'gid' => $gid));
                } else echo 'error';
                {
                    Yii::app()->user->setFlash('danger', 'Коментариев нет');
                }

            }
        }
    }

    public function actiongroupadd()
    {
        if (Yii::app()->request->isAjaxRequest) {
            $model = UserGroup::model()->find('idUser = :id and url = :url', array(':id' => Yii::app()->user->id, ':url' => Yii::app()->getRequest()->getParam('url')));
            if (count($model) == 0) {
                $res = new UserGroup;
                $this->performAjaxValidation($res, 'group-form');
                $res->idUser = Yii::app()->user->id;
                $res->url = $_POST['UserGroup']['url'];
                $res->nameUrl = $_POST['UserGroup']['nameUrl'];
                if ($res->save()) {
                    echo 'Данные сохранены';

                }
            }
        }
    }

    public function actiondeletegroup()
    {
        $url = Yii::app()->getRequest()->getParam('url');
        preg_match('/vk.com\/(.*?)$/', $url, $matches);
        $url = $matches[1];
        $res = SendHelper::send('https://api.vk.com/method/groups.getById?group_ids=' . $url . '&fields=description', '', '', '', true);
        preg_match('/gid":(.*?),/', $res, $matches);
        echo $matches[1];
        $url1 = Yii::app()->getRequest()->getParam('url');
        Wall::model()->with(array('Settings' => array('condition' => 'Settings.idUser=:id', 'params' => array(':id' => Yii::app()->user->id))))->deleteAll('id_group=' . $matches[1]);
        UserGroup::model()->deleteAll('idUser = :id and url = :url', array(':id' => Yii::app()->user->id, ':url' => $url1));
        $this->redirect(Yii::app()->request->getBaseUrl(true));
    }

    public function actiongroup()
    {
        $token = User::model()->find('idUser=:myParams', array(':myParams' => Yii::app()->user->id));
        $url = Yii::app()->getRequest()->getParam('url');
        $http = Yii::app()->getRequest()->getParam('url');
        $button = Yii::app()->getRequest()->getParam('but');
        $response = UserGroup::model()->find('idUser = :id and url = :url', array(':id' => Yii::app()->user->id, ':url' => $url));
        if (count($response) == 0)
            $this->redirect(Yii::app()->getRequest()->getBaseUrl(true));

        $name = $response->nameUrl;
        preg_match('/vk.com\/(.*?)$/', $url, $matches);
        if (!isset($matches[1]))
            $this->redirect(Yii::app()->getRequest()->getBaseUrl(true));

        $url = $matches[1];
        $res = SendHelper::send('https://api.vk.com/method/groups.getById?group_ids=' . $url . '&fields=description', '', '', '', true);
        $res = json_decode($res, 1);
        if (isset($res['response'])) {
            $nameGroup = $res['response'][0]['name'];
            $idGroup = $res['response'][0]['gid'];
            $res = SendHelper::send('https://api.vk.com/method/photos.getAlbums?owner_id=-' . $idGroup . '&need_covers=1&photo_sizes=1', '', '', '', true);
            $res = json_decode($res, 1);
            if (isset($res['response'])) {
                for ($i = 0; $i < count($res['response']); $i++) {
                    $image = $res['response'][$i]['sizes'][1]['src'];
                    $title = $res['response'][$i]['title'];
                    $aid = $res['response'][$i]['aid'];
                    $r = Wall::model()->with(array('Settings' => array('condition' => 'Settings.idUser=:id', 'params' => array(':id' => Yii::app()->user->id))))->count('tipeText=1 and id_group=' . $idGroup . ' and idAlbum=' . $aid);
                    $items[] = array('id' => $aid, 'image' => $image, 'nameGroup' => $nameGroup, 'idGroup' => $idGroup, 'title' => $title, 'name' => $name, 'count' => $r);
                }

                $resp = Settings::model()->find('idUser = :id', array(':id' => Yii::app()->user->id));
                if ($button == '1') {
                    $news = Yii::app()->getRequest()->getParam('news');
                    $wall = Yii::app()->getRequest()->getParam('wall');
                    $resp->postImg = (int)$_POST['Settings']['postImg'];
                    $resp->DateWallTo = $_POST['Settings']['DateWallTo'];
                    $resp->DateWallFrom = $_POST['Settings']['DateWallFrom'];

                    $str = '';
                    $st = '';

                    for ($z = 0; $z < count($wall); $z++) {
                        $command = "/usr/local/php/bin/php -f " . Yii::app()->params['path'] . "/protected/cron.php Wall " . Yii::app()->user->id . ' ' . Yii::app()->getRequest()->getParam('url');
                        $process = new Process($command);
                        if ($z == count($news) - 1) $st .= $process->getPid();
                        else $st .= $process->getPid();
                    }

                    $resp->pidWall = $st;
                    $resp->save();

                    for ($x = 0; $x < count($news); $x++) {
                        $command = "/usr/local/php/bin/php -f " . Yii::app()->params['path'] . "/protected/cron.php Comment " . Yii::app()->user->id . ' ' . Yii::app()->getRequest()->getParam('url') . ' ' . $news[$x];
                        $process = new Process($command);
                        if ($x == count($news) - 1) $str .= $process->getPid();
                        else $str .= $process->getPid() . '_';
                    }

                    $resp->pidComment = $str;
                    $resp->save();
                    $this->redirect(Yii::app()->request->getBaseUrl(true) . '/site/group?url=' . Yii::app()->getRequest()->getParam('url'));

                } elseif ($button == '2') {
                    $resp = Settings::model()->find('idUser = :id', array(':id' => Yii::app()->user->id));
                    $process = new Process;
                    $pid = $resp->pidComment;
                    $pidWall = $resp->pidWall;
                    $pid = explode("_", $pid);
                    for ($j = 0; $j < count($pid); $j++) {
                        $process->setPid($pid[$j]);
                        $process->stop();
                    }
                    $process = new Process;
                    $process->setPid($pidWall);
                    $process->stop();
                    $resp->pidWall = NULL;
                    $resp->save();
                    $resp->pidComment = NULL;
                    $resp->save();
                }
                $respon = Wall::model()->with(array('Settings' => array('condition' => 'Settings.idUser=:id', 'params' => array(':id' => Yii::app()->user->id))))->findAll('tipeText=1 and id_group=' . $idGroup);
                $countwal = Wall::model()->with(array('Settings' => array('condition' => 'Settings.idUser=:id', 'params' => array(':id' => Yii::app()->user->id))))->count('tipeText=3 and id_group=' . $idGroup);
                $res = SendHelper::send('https://api.vk.com/method/groups.getById?group_ids=' . $url . '&fields=description', '', '', '', true);
                preg_match('/name":(.*?),/', $res, $mat);
                $nameGroup = $mat[1];

                $items1 = array();
                for ($j = 0; $j < count($respon); $j++) {
                    $nameUser = $respon[$j]->nameUser;
                    $text = $respon[$j]->textWall;
                    $data = $respon[$j]->dataWall;
                    $photo = $respon[$j]->photoWall;
                    $user = $respon[$j]->idUser;
                    $name = $respon[$j]->nameAlbum;
                    $nameAlbum = $respon[$j]->nameUser;
                    $items1[] = array('id' => $j, 'date' => $data, 'photo' => $photo, 'text' => $text, 'user' => $user, 'nameAlbum' => $nameAlbum, 'name' => $name);
                }
                $Provider = new CArrayDataProvider($items1, array());
                $itemsProvider = new CArrayDataProvider($items, array());
                $pidComment = $resp->pidComment;
                $urlPage = Yii::app()->getRequest()->getParam('url');
                $this->render('groups', array('model' => $resp, 'itemsProvider' => $itemsProvider, 'countwal' => $countwal, 'url' => $http, 'urlPage' => $urlPage,
                    'pidComment' => $pidComment, 'idGroup' => $idGroup, 'Provider' => $Provider, 'nameGroup' => $nameGroup));
            } else {
                $this->redirect(Yii::app()->request->getBaseUrl(true));
            }
        }
    }

    public function actionrefresh()
    {
        if (Yii::app()->request->isAjaxRequest) {
            $token = User::model()->find('idUser=:myParams', array(':myParams' => Yii::app()->user->id));
            $url = Yii::app()->getRequest()->getParam('url');
            $button = Yii::app()->getRequest()->getParam('but');
            $http = Yii::app()->getRequest()->getParam('url');
            $response = UserGroup::model()->find('idUser = :id and url = :url', array(':id' => Yii::app()->user->id, ':url' => Yii::app()->getRequest()->getParam('url')));
            $name = $response->nameUrl;
            preg_match('/vk.com\/(.*?)$/', $url, $matches);
            $url = $matches[1];
            $res = SendHelper::send('https://api.vk.com/method/groups.getById?group_ids=' . $url . '&fields=description', '', '', '', true);
            preg_match('/gid":(.*?),/', $res, $matches);
            $idGroup = $matches[1];
            $res = SendHelper::send('https://api.vk.com/method/photos.getAlbums?owner_id=-' . $idGroup . '&need_covers=1&photo_sizes=1', '', '', '', true);
            $res = json_decode($res, true);
            echo 'good';
            for ($i = 0; $i < count($res["response"]); $i++) {
                $image = $res["response"][$i]['sizes'][1]['src'];
                $title = $res["response"][$i]['title'];
                $aid = $res["response"][$i]['aid'];

                $r = Wall::model()->with(array('Settings' => array('condition' => 'Settings.idUser=:id', 'params' => array(':id' => Yii::app()->user->id))))->count('tipeText=1 and id_group=' . $idGroup . ' and idAlbum=' . $aid);
                var_dump($r);
                $items[] = array('id' => $aid, 'image' => $image, 'title' => $title, 'name' => $name, 'count' => $r);
            }
        }
    }

    public function actionselectgroup()
    {
        if (Yii::app()->request->isAjaxRequest) {
            $select = Yii::app()->getRequest()->getParam('type');
            $aid = Yii::app()->getRequest()->getParam('aid');
            if (empty($select)) $select = 0;
            if ($select <= 3 && $select >= 0) {
                $dataProvider = new CActiveDataProvider('Wall', array(
                    'criteria' => array(
                        'order' => 't.dataWall desc',
                        'params' => array(':id' => Yii::app()->user->id, ':type' => $select, ':idalbum' => $aid),
                        'with' => array(
                            'Settings' => array(
                                'condition' => 'Settings.idUser=:id and  t.tipeSelect = :type and t.idAlbum= :idalbum',
                            ),
                        ),
                    ),
                    'pagination' => array(
                        'pageSize' => 15
                    ),
                ));
                $this->renderPartial('list', array('dataProvider' => $dataProvider, 'aid' => $aid), false, true);
            }
        } else {
            if (Yii::app()->getRequest()->getParam('but') == '2' && Yii::app()->getRequest()->getParam('metka') == 'metka-spam') {
                $news = Yii::app()->getRequest()->getParam('news');
                $aid = Yii::app()->getRequest()->getParam('aid');
                for ($x = 0; $x < count($news); $x++) {
                    $respon = Wall::model()->with(array('Settings' => array('condition' => 'Settings.idUser=:id', 'params' => array(':id' => Yii::app()->user->id))))->find('idAlbum=' . $aid . ' and idWall=' . $news[$x]);
                    $respon->tipeSelect = '1';
                    $respon->save();
                }
                $this->redirect(Yii::app()->request->getBaseUrl(true) . '/site/SelectGroup?aid=' . $aid);
            }

            if (Yii::app()->getRequest()->getParam('but') == '2' && Yii::app()->getRequest()->getParam('metka') == 'metka-read') {
                $news = Yii::app()->getRequest()->getParam('news');
                $aid = Yii::app()->getRequest()->getParam('aid');
                for ($x = 0; $x < count($news); $x++) {
                    $respon = Wall::model()->with(array('Settings' => array('condition' => 'Settings.idUser=:id', 'params' => array(':id' => Yii::app()->user->id))))->find('idAlbum=' . $aid . ' and idWall=' . $news[$x]);
                    $respon->tipeSelect = '2';
                    $respon->save();
                }
                $this->redirect(Yii::app()->request->getBaseUrl(true) . '/site/SelectGroup?aid=' . $aid);
            }

            if (Yii::app()->getRequest()->getParam('but') == '2' && Yii::app()->getRequest()->getParam('metka') == 'metka-select') {
                $news = Yii::app()->getRequest()->getParam('news');
                $aid = Yii::app()->getRequest()->getParam('aid');
                for ($x = 0; $x < count($news); $x++) {
                    $respon = Wall::model()->with(array('Settings' => array('condition' => 'Settings.idUser=:id', 'params' => array(':id' => Yii::app()->user->id))))->find('idAlbum=' . $aid . ' and idWall=' . $news[$x]);
                    $respon->tipeSelect = '3';
                    $respon->save();
                }
                $this->redirect(Yii::app()->request->getBaseUrl(true) . '/site/SelectGroup?aid=' . $aid);
            }

            if (Yii::app()->getRequest()->getParam('but') == '3') {
                $news = Yii::app()->getRequest()->getParam('news');
                $aid = Yii::app()->getRequest()->getParam('aid');
                for ($x = 0; $x < count($news); $x++) {
                    Wall::model()->with(array('Settings' => array('condition' => 'Settings.idUser=:id', 'params' => array(':id' => Yii::app()->user->id))))->deleteAll('idAlbum=' . $aid . ' and idWall=' . $news[$x]);
                }
                $this->redirect(Yii::app()->request->getBaseUrl(true) . '/site/SelectGroup?aid=' . $aid);
            }

            if (Yii::app()->getRequest()->getParam('but') == '1') {
                $edit = Yii::app()->getRequest()->getParam('textfield');
                $aid = Yii::app()->getRequest()->getParam('aid');
                $respon = Wall::model()->with(array('Settings' => array('condition' => 'Settings.idUser=:id', 'params' => array(':id' => Yii::app()->user->id))))->find('idAlbum=' . $aid);
                $nameAlbum = $respon->nameUser;
                $dataProvider = new CActiveDataProvider('Wall', array(
                    'criteria' => array(
                        'order' => 't.IdWall desc',
                        'condition' => 't.textWall LIKE :word',
                        'params' => array(':word' => '%' . $edit . '%'),
                        'with' => array(
                            'Settings' => array(
                                'condition' => 'Settings.idUser=:id and t.idAlbum= :idalbum',
                                'params' => array(':id' => Yii::app()->user->id, ':idalbum' => $aid),
                            ),
                        ),
                    ),
                    'pagination' => array(
                        'pageSize' => 15
                    ),
                ));
                $this->render('selectgroup', array('dataProvider' => $dataProvider, 'nameAlbum' => $nameAlbum, 'aid' => $aid));
            } else {
                $aid = Yii::app()->getRequest()->getParam('aid');

                $respon = Wall::model()->with(array('Settings' => array('condition' => 'Settings.idUser=:id', 'params' => array(':id' => Yii::app()->user->id))))->find('idAlbum=' . $aid);
                if (count($respon) > 0) {
                    $nameAlbum = $respon->nameUser;
                    $dataProvider = new CActiveDataProvider('Wall', array(
                        'criteria' => array(
                            'order' => 't.dataWall desc',
                            'params' => array(':id' => Yii::app()->user->id, ':idalbum' => $aid),
                            'with' => array(
                                'Settings' => array(
                                    'condition' => 'Settings.idUser=:id and t.idAlbum= :idalbum',
                                ),
                            ),
                        ),
                        'pagination' => array(
                            'pageSize' => 15
                        ),
                    ));
                    $this->render('selectgroup', array('dataProvider' => $dataProvider, 'nameAlbum' => $nameAlbum, 'aid' => $aid));
                } else echo 'error';
            }
        }
    }

    public function actionPayment()
    {
        if (!Yii::app()->user->isGuest) {
            $model = new Pay;
            $model->unsetAttributes();
            if (isset($_GET['Pay'])) {
                if ($_GET['Pay']['type_pay'] == '' || $_GET['Pay']['sum'] == '') {
                    Yii::app()->user->setFlash('danger', 'Выберите тип оплаты и/или введите сумму платежа');
                    $this->redirect(Yii::app()->request->getBaseUrl(true) . '/site/payment/');
                } else {
                    $userId = Yii::app()->user->id;
                    switch ($_GET['Pay']['type_pay']) {
                        case '0':
                            $params = '?price=' . $_GET['Pay']['sum'];
                            $params .= '&id=' . $userId;
                            $params .= 'type=wm';
                            $this->redirect('http://openpp.ru/pay/socsearch/' . $params);
                            break;
                        case '1':
                            $comment = urlencode('Пополнение баланса SocSearch.info');
                            $params = 'receiver=410013357067943';
                            $params .= '&formcomment=' . $comment;
                            $params .= '&short-dest=socsearch';
                            $params .= '&writable-targets=false';
                            $params .= '&quickpay-form=shop';
                            $params .= '&targets=' . $userId;
                            $params .= '&sum=' . $_GET['Pay']['sum'];
                            $params .= '&paymentType=PC';
                            $params .= '&label=' . $userId;
                            $this->redirect('https://money.yandex.ru/quickpay/confirm.xml?' . $params);
                            break;
                        case '2':
                            $mrh_login = Yii::app()->params['robokassa'][0];
                            $mrh_pass1 = Yii::app()->params['robokassa'][1];
                            $inv_id = 0;
                            $inv_desc = 'Пополнение баланса ClientFinder';
                            $def_sum = $_GET['Pay']['sum'];
                            $id = Yii::app()->user->id;
                            $culture = 'ru';
                            $encoding = 'utf-8';
                            $crc = md5("$mrh_login:$def_sum:$inv_id:$mrh_pass1:shpId=$id");
                            $this->redirect("https://auth.robokassa.ru/Merchant/Index.aspx?" . "MrchLogin=$mrh_login&OutSum=$def_sum&InvId=$inv_id" . "&Desc=$inv_desc&SignatureValue=$crc&shpId=$id" . "");
                            break;
                    }
                }
            } else
                $this->render('pay', array('model' => $model));
        } else
            $this->redirect(Yii::app()->request->getBaseUrl(true));
    }

    /* Оплата WebMoney */
    public function actionOpenPP()
    {
        if ((!empty($_POST['rand']))
            && (!empty($_POST['price']))
            && (!empty($_POST['hash']))
            && (!empty($_POST['id']))
            && (md5('id=' . trim($_POST['id']) . 'rand=' . trim($_POST['rand']) . 'price=' . trim($_POST['price']) . 'wm='
                    . trim($_POST['wm']) . Yii::app()->params['openpp']['secret_key']) == trim($_POST['hash']))
        ) {
            $pay = new Pay;
            $pay->user_id = $_POST['id'];
            $pay->type = 1;
            $pay->type_pay = 0;
            $pay->sum = $_POST['price'];
            $pay->date = date('Y-m-d H:i');
            $pay->save();

            $user = User::model()->findByPk($_POST['id']);
            $user->userBalance += $_POST['price'];
            $user->save();
        }
    }

    /* Редирект покупателя на сайт Робокассы для оплаты предмета */
    public function actionPay()
    {
        $id = Yii::app()->user->id;
        $price = 0.01;
        if (!empty($id)) {
            $mrh_login = Yii::app()->params['robokassa'][0];
            $mrh_pass1 = Yii::app()->params['robokassa'][1];
            $inv_id = 0;
            $inv_desc = 'Пополнение баланса ClientFinder';
            $def_sum = $price;
            $culture = 'ru';
            $encoding = 'utf-8';
            $crc = md5("$mrh_login:$def_sum:$inv_id:$mrh_pass1:shpId=$id");
            $this->redirect("https://auth.robokassa.ru/Merchant/Index.aspx?" . "MrchLogin=$mrh_login&OutSum=$def_sum&InvId=$inv_id" . "&Desc=$inv_desc&SignatureValue=$crc&shpId=$id" . "");
        } else $this->redirect(Yii::app()->request->getBaseUrl(true));
    }

    /* Проверка оплаты, запись истории */
    public function actionRobokassa()
    {
        $mrh_pass2 = Yii::app()->params['robokassa'][2];

        // HTTP parameters:
        $out_summ = $_REQUEST["OutSum"];
        $inv_id = $_REQUEST["InvId"];
        $shpId = $_REQUEST['shpId']; // User ID
        $crc = $_REQUEST["SignatureValue"];
        $crc = strtoupper($crc);

        $my_crc = strtoupper(md5("$out_summ:$inv_id:$mrh_pass2:shpId=$shpId"));

        if (strtoupper($my_crc) != strtoupper($crc)) {
            echo "bad sign\n";
            exit();
        }
        echo "OK$inv_id\n";

        $pay = new Pay;
        $pay->user_id = $shpId;
        $pay->sum = $out_summ;
        $pay->type = 1;
        $pay->type_pay = 2;
        $pay->date = date('Y-m-d H:i:s');
        $data = 'UID: ' . $shpId . ' Date: ' . date('Y-m-d H:i:s') . ' Sum: ' . $out_summ . "\r\n";
        if ($pay->validate()) $pay->save();
        else file_put_contents('robokassa.txt', $data . var_export($pay->getErrors(), 1) . "\r\n", FILE_APPEND);

        $user = User::model()->findByPk($shpId);
        $user->userBalance += $out_summ;
        $user->save();
    }

    public function actionYandex()
    {
        $r = array(
            'notification_type' => Yii::app()->getRequest()->getParam('notification_type'),
            'operation_id' => Yii::app()->getRequest()->getParam('operation_id'),
            'amount' => Yii::app()->getRequest()->getParam('amount'),
            'currency' => Yii::app()->getRequest()->getParam('currency'),
            'datetime' => Yii::app()->getRequest()->getParam('datetime'),
            'sender' => Yii::app()->getRequest()->getParam('sender'),
            'codepro' => Yii::app()->getRequest()->getParam('codepro'),
            'label' => Yii::app()->getRequest()->getParam('label'),
            'sha1_hash' => Yii::app()->getRequest()->getParam('sha1_hash'),
        );

        if (sha1($r['notification_type'] . '&' .
                $r['operation_id'] . '&' .
                $r['amount'] . '&' .
                $r['currency'] . '&' .
                $r['datetime'] . '&' .
                $r['sender'] . '&' .
                $r['codepro'] . '&' .
                Yii::app()->params['yandex']['secret'] . '&' .
                $r['label']) == $r['sha1_hash']
        ) {
            //получаем данные
            $userId = $r['label'];
            $amount = floatval(Yii::app()->getRequest()->getParam('withdraw_amount'));
            $pay = new Pay;
            $pay->user_id = $userId;
            $pay->type = 1;
            $pay->type_pay = 1;
            $pay->sum = $amount;
            $pay->date = date('Y-m-d H:i:s');
            $pay->save();

            $user = User::model()->findByPk($userId);
            $user->userBalance += $amount;
            $user->save();
        }
    }

    /* Уведомление в случае успешного платежа */
    public function actionSuccess()
    {
        if (isset($_REQUEST['OutSumm'])) {
            // as a part of SuccessURL script
            // your registration data
            $mrh_pass1 = Yii::app()->params['robokassa'][1];  // merchant pass1 here

            // HTTP parameters:
            $out_summ = $_REQUEST["OutSum"];
            $inv_id = $_REQUEST["InvId"];
            $shpId = $_REQUEST['shpId']; // User ID
            $crc = $_REQUEST["SignatureValue"];
            $crc = strtoupper($crc);  // force uppercase

            // build own CRC
            $my_crc = strtoupper(md5("$out_summ:$inv_id:$mrh_pass1:shpId=$shpId"));
            if (strtoupper($my_crc) != strtoupper($crc)) {
                echo "bad sign\n";
                exit();
            }

            // you can check here, that resultURL was called
            // (for better security)
            // OK, payment proceeds
            echo "Thank you for using our service\n";
            if (!Yii::app()->user->isGuest && isset($shpId)) {
                $this->render('success');
            }
        } elseif (isset($_REQUEST['type']) && $_REQUEST['type'] == 'wm') {
            $this->render('success');
        } else
            $this->redirect(Yii::app()->request->getBaseUrl(true));
    }

    /* Уведомление в случае отказа от платежа */
    public function actionFail()
    {
        if (!Yii::app()->user->isGuest) {
            $this->render('fail');
        } else
            $this->redirect(Yii::app()->request->getBaseUrl(true));
    }

    public function actionexportAlbum()
    {

        $type = Yii::app()->getRequest()->getParam('type');

        $aid = Yii::app()->getRequest()->getParam('id');

        if ($type == 'txt') {

            $response = Wall::model()->with(array('Settings' => array('condition' => 'Settings.idUser=:id', 'params' => array(':id' => Yii::app()->user->id))))->findAll('tipeText=1 and idAlbum=' . $aid);


            $res = User::model()->findAll('idUser = :id', array(':id' => Yii::app()->user->id));
            $model = Settings::model()->find('idUser = :id', array(':id' => Yii::app()->user->id));
            $model->nameTxt = NULL;
            if ($model->nameTxt == NULL) {
                $log = '';
                for ($i = 0; $i < count($response); $i++) {
                    $log .= 'Дата:' . $response[$i]->dataWall . "\r\n";
                    $log .= 'Ссылка пользователя: https://vk.com/id' . $response[$i]->idUser . "\r\n";
                    $log .= 'Фото:' . $response[$i]->photoWall . "\r\n";
                    $log .= 'Текст:' . $response[$i]->textWall . "\r\n";
                    $log .= "\r\n" . '========================================================================' . "\r\n\r\n";
                }
                $model->nameTxt = $res[0]->userFirstName . '_' . $res[0]->userLastName . '_' . date('Y-m-d H:i:s') . '.txt';

                file_put_contents('export/' . $res[0]->userFirstName . '_' . $res[0]->userLastName . '_' . date('Y-m-d H:i:s') . '.txt', $log);

                $model->dateTxt = date('Y-m-d H:i:s');
                $model->save();
                $this->redirect(Yii::app()->request->getBaseUrl(true) . '/export/' . $model->nameTxt);
            } elseif ($model->nameTxt != NULL) {
                echo 'error';
            }

        } elseif ($type == 'xls') {

            $xls = '';
            $response = Wall::model()->with(array('Settings' => array('condition' => 'Settings.idUser=:id', 'params' => array(':id' => Yii::app()->user->id))))->findAll('tipeText=1 and idAlbum=' . $aid);
            $model = Settings::model()->find('idUser = :id', array(':id' => Yii::app()->user->id));
            $res = User::model()->findAll('idUser = :id', array(':id' => Yii::app()->user->id));
            $model->nameXls = NULL;
            if ($model->nameXls == NULL) {
                for ($i = 0; $i < count($response); $i++) {
                    $xls .= 'Дата:' . $response[$i]->dataWall . "\n";
                    $xls .= 'Ссылка пользователя: https://vk.com/id' . $response[$i]->idUser . "\n";
                    $xls .= 'Фото:' . $response[$i]->photoWall . "\n";
                    $xls .= 'Текст:' . $response[$i]->textWall . "\n\n";
                }
                $xls = iconv("UTF-8", "CP1251//IGNORE", $xls);

                file_put_contents('export/' . $res[0]->userFirstName . '_' . $res[0]->userLastName . '_' . date('Y-m-d H:i:s') . '.csv', $xls);
                $model->dateXls = date('Y-m-d H:i:s');
                $model->nameXls = $res[0]->userFirstName . '_' . $res[0]->userLastName . '_' . date('Y-m-d H:i:s') . '.csv';
                $model->save();
                $this->redirect(Yii::app()->request->getBaseUrl(true) . '/export/' . $model->nameXls);
            } else {
                echo 'error';
            }
        }

        if ($type == 'walltxt') {

            $response = Wall::model()->with(array('Settings' => array('condition' => 'Settings.idUser=:id', 'params' => array(':id' => Yii::app()->user->id))))->findAll('tipeText=3 and id_group=' . $aid);


            $res = User::model()->findAll('idUser = :id', array(':id' => Yii::app()->user->id));
            $model = Settings::model()->find('idUser = :id', array(':id' => Yii::app()->user->id));
            $model->nameTxt = NULL;
            if ($model->nameTxt == NULL) {
                $log = '';
                for ($i = 0; $i < count($response); $i++) {
                    $log .= 'Дата:' . $response[$i]->dataWall . "\r\n";
                    $log .= 'Ссылка пользователя: https://vk.com/id' . $response[$i]->idUser . "\r\n";
                    $log .= 'Фото:' . $response[$i]->photoWall . "\r\n";
                    $log .= 'Текст:' . $response[$i]->textWall . "\r\n";
                    $log .= "\r\n" . '========================================================================' . "\r\n\r\n";
                }
                file_put_contents('export/' . $res[0]->userFirstName . '_' . $res[0]->userLastName . '_' . date('Y-m-d H:i:s') . '.txt', $log);
                $model->nameTxt = $res[0]->userFirstName . '_' . $res[0]->userLastName . '_' . date('Y-m-d H:i:s') . '.txt';
                $model->dateTxt = date('Y-m-d H:i:s');
                $model->save();
                $this->redirect(Yii::app()->request->getBaseUrl(true) . '/export/' . $model->nameTxt);
            } elseif ($model->nameTxt != NULL) {
                echo 'error';
            }

        } elseif ($type == 'wallxls') {

            $xls = '';
            $response = Wall::model()->with(array('Settings' => array('condition' => 'Settings.idUser=:id', 'params' => array(':id' => Yii::app()->user->id))))->findAll('tipeText=3 and id_group=' . $aid);
            $model = Settings::model()->find('idUser = :id', array(':id' => Yii::app()->user->id));
            $res = User::model()->findAll('idUser = :id', array(':id' => Yii::app()->user->id));
            $model->nameXls = NULL;
            if ($model->nameXls == NULL) {
                for ($i = 0; $i < count($response); $i++) {
                    $xls .= 'Дата:' . $response[$i]->dataWall . "\n";
                    $xls .= 'Ссылка пользователя: https://vk.com/id' . $response[$i]->idUser . "\n";
                    $xls .= 'Фото:' . $response[$i]->photoWall . "\n";
                    $xls .= 'Текст:' . $response[$i]->textWall . "\n\n";
                }
                $xls = iconv("UTF-8", "CP1251//IGNORE", $xls);

                file_put_contents('export/' . $res[0]->userFirstName . '_' . $res[0]->userLastName . '_' . date('Y-m-d H:i:s') . '.csv', $xls);
                $model->dateXls = date('Y-m-d H:i:s');
                $model->nameXls = $res[0]->userFirstName . '_' . $res[0]->userLastName . '_' . date('Y-m-d H:i:s') . '.csv';
                $model->save();
                $this->redirect(Yii::app()->request->getBaseUrl(true) . '/export/' . $model->nameXls);

            } else {
                echo 'error';
            }
        }


        $command = "/usr/local/php/bin/php -f " . Yii::app()->params['path'] . "/protected/cron.php Export " . Yii::app()->user->id;
        $process = new Process($command);

    }
}
    


    

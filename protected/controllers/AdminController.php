<?php

class AdminController extends Controller
{
    public function actionIndex() {
        if (Yii::app()->user->isUser('Администраторы')) {
            $this->layout = 'admin';
            $info = array(
                'user' => User::model()->count(),
                'pay' => Pay::model()->count(),
                'down' => round(Statistics::getInfo(date('Y-m-d'), 'moneyDown'), 1),
                'up' => round(Statistics::getInfo(date('Y-m-d'), 'moneyUp'), 1)
            );
            $this->render('index', array('info' => $info));
        } else
            $this->redirect(Yii::app()->request->getBaseUrl(true));
    }

	public function actionPay()
	{
        /**
         *    type                   type_pay
         * -------------------------------------
         *   Списано      - 0 -      WebMoney
         *   Внесено      - 1 -       Yandex
         *  - empty -     - 2 -      Robokassa
         *  - empty -     - 3 -   Ручное пополнение
         */

        $this->layout = 'admin';
        if (Yii::app()->user->isUser('Администраторы')) {
            $model = new Pay('search');
            $model->unsetAttributes();
            if(isset($_GET['Pay']))
                $model->attributes = $_GET['Pay'];
            $this->render('pay', array('model'=>$model));
        } else
            $this->redirect(Yii::app()->request->getBaseUrl(true));
	}

    public function actionStatistics() {
        $this->layout = 'admin';
        if (Yii::app()->user->isUser('Администраторы')) {
        $model = new Statistics('search');
        $model->unsetAttributes();
        if(isset($_GET['Statistics']))
        {
            $model->attributes = $_GET['Statistics'];
        }
        $this->render('statistics', array('model'=>$model));
        } else
            $this->redirect(Yii::app()->request->getBaseUrl(true));
    }
}
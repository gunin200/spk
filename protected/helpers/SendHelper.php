<?php

class SendHelper

{



    public static function send($url,$param=null,$cookie=null,$referer=null,$body1=false,$isProxy=false,$proxy='',$proxyType='',$proxyAuth=''){
        $ch = curl_init();
       
        curl_setopt($ch, CURLOPT_URL, $url);
 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        //curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT,30);

      curl_setopt($ch, CURLOPT_TIMEOUT, 1);
         if ($isProxy)
        {
            $x=file_get_contents('/home/wwwroot/clientfinder.ru/proxy_http_ip.txt');
            $y=explode("\n", $x);
            curl_setopt($ch, CURLOPT_PROXY, $y[rand(0,count($y)-1)]); 
        }
       
        if ($proxy!=''){

            curl_setopt($ch, CURLOPT_PROXY, "$proxy");

           // curl_setopt($ch, CURLOPT_PROXYTYPE, "CURLPROXY_SOCKS5");

            if ($proxyAuth!=''){

                curl_setopt($ch, CURLOPT_PROXYUSERPWD, "$proxyAuth");

            }

        }

        curl_setopt($ch, CURLOPT_HEADER, true);

        curl_setopt($ch, CURLOPT_REFERER, $referer);

        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);

        curl_setopt($ch, CURLOPT_COOKIE, $cookie);  //

        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; ru; rv:1.9.0.1) Gecko/2008070208');

        if (trim($param)!=''){

            curl_setopt($ch, CURLOPT_POST, 1);

            curl_setopt($ch, CURLOPT_POSTFIELDS, $param);

        } else {

            curl_setopt($ch, CURLOPT_POST, 0);

        }



        $result=curl_exec($ch);
     


        $header=substr($result,0,curl_getinfo($ch,CURLINFO_HEADER_SIZE));

        $body=substr($result,curl_getinfo($ch,CURLINFO_HEADER_SIZE));

        //echo $header;

        preg_match_all("/Set-Cookie: (.*?)=(.*?);/i",$header,$res);

        $cookie='';

        foreach ($res[1] as $key => $value) {

            $cookie.= $value.'='.$res[2][$key].'; ';

        };

        curl_close($ch);

        if ($body1 == false)

        return $cookie."@@@".$result;

    elseif($body1 == true)

        return $body;

    }



    public static function curl($url,$headers=null,$param=null){

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        //curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT,30);

        curl_setopt($ch, CURLOPT_TIMEOUT, 30);



        curl_setopt($ch, CURLOPT_HEADER, 1);

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);



        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; ru; rv:1.9.0.1) Gecko/2008070208');

        if (trim($param)!=''){

            curl_setopt($ch, CURLOPT_POST, 1);

            curl_setopt($ch, CURLOPT_POSTFIELDS, $param);

        } else {

            curl_setopt($ch, CURLOPT_POST, 0);

        }



        $result=curl_exec($ch);



        $header=substr($result,0,curl_getinfo($ch,CURLINFO_HEADER_SIZE));

        $body=substr($result,curl_getinfo($ch,CURLINFO_HEADER_SIZE));

        //echo $header;

        preg_match_all("/Set-Cookie: (.*?)=(.*?);/i",$header,$res);

        $cookie='';

        foreach ($res[1] as $key => $value) {

            $cookie.= $value.'='.$res[2][$key].'; ';

        };

        curl_close($ch);



        return $cookie."@@@".$result;

    }



    public static function data_hash_walk(&$field)

    {

        if($field === 'notification_secret')

            $field = Yii::app()->params['yandex']['secret'] ;

        else

            $field = @$_POST[$field];

    }

}
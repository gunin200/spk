<?php

class UserBehavior extends CBehavior {
    public function isUser($role) {
        if (!Yii::app()->user->isGuest) {
            Yii::import('application.models.User');
            $user = User::model()->findByAttributes(array('idUser' => Yii::app()->user->id));
            if ($user->role == $role) return true;
            else return false;
        }
    }
}

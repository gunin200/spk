<?

class AdminController extends CController {

    public function actionUser() {
        $this->layout = '//layouts/admin';
        Yii::import('application.models.User');
        if (Yii::app()->user->isUser('Администраторы')) {
            $action = Yii::app()->getRequest()->getParam('user');
            $id = Yii::app()->getRequest()->getParam('id');
            $user = User::model()->findByPk($id);
            $this->performAjaxValidation(User::model(), 'edit-form');

            if ($action == 'view') $this->render('view', array('user' => $user));
            elseif ($action == 'edit') $this->render('update', array('user' => $user));
            elseif (isset($_POST['User'])) {
                $user = User::model()->findByPk($_POST['idUser']);
                $user->attributes = $_POST['User'];
                if ($user->save()) {
                    Yii::app()->user->setFlash('status', 'Изменения успешно сохранены!');
                    $this->redirect(Yii::app()->request->getBaseUrl(true) . '/admin/user');
                }
            } else {
                $model = new User('search');
                $model->unsetAttributes();
                if (isset($_GET['User'])) $model->attributes = $_GET['User'];
                $this->render('index', array('model' => $model));
            }
        } else
            $this->redirect(Yii::app()->request->getBaseUrl(true));
    }

    protected function performAjaxValidation($model, $form)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === $form) {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}
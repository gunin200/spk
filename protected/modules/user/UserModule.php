<?php

class UserModule extends CWebModule
{

    public function init()
    {
        $import = array(
            'user.models.*',
            'user.components.*',
        );

        if (Yii::app()->hasModule('partner')){
            $import[] = 'application.modules.partner.models.*';
        }

        $this->setImport(
            $import
        );
    }

//    public static function rules()
//    {
//        return array(
//            '',
//            '<action:login|logout|registration>' => 'user/default/<action>',
//            'user/list/index/act/<act:view|update|delele>/id/<id:[\d]+>'=>'user/list/index/',
//            'user/list/task/act/<act:view|update|delele>/id/<id:[\d]+>'=>'user/list/task/',
//        );
//    }
}

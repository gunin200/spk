<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Пользователи</h1>

        <?$this->widget('zii.widgets.CBreadcrumbs', array(
            'htmlOptions' => array(
                'class' => 'breadcrumb'
            ),
            'separator' => ' / ',
            'links' => array(
                'Администрирование' => array('../admin'),
                'Пользователи'
            )
        ));?>

        <? if (Yii::app()->user->hasFlash('status')): ?>
            <div class="alert alert-success" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <?= Yii::app()->user->getFlash('status'); ?>
            </div>
        <? endif; ?>

        <?php $this->widget('zii.widgets.grid.CGridView', array(
            'id' => 'user-grid',
            'dataProvider' => $model->search(),
            'filter' => $model,
            'itemsCssClass' => 'table table-hover',
            'columns' => array(
                'idUser',
                'userFirstName',
                'userLastName',
                'userBalance',
                array(
                    'name' => 'role',
                    'filter' => array('Пользователи' => 'Пользователи', 'Администраторы' => 'Администраторы'),
                ),
                array(
                    'class' => 'CButtonColumn',
                    'header' => 'Действия',
                    'template' => '{view} {update}',
                    'buttons' => array(
                        'view' => array(
                            'label' => 'Просмотр',
                            'url' => 'Yii::app()->createUrl("admin/user/view/", array("id"=>$data->idUser))',
                        ),
                        'update' => array(
                            'label' => 'Обновить',
                            'url' => 'Yii::app()->createUrl("admin/user/edit/", array("id"=>$data->idUser))',
                        ),
                    ),
                ),
            ),
        )); ?>
    </div>
</div>
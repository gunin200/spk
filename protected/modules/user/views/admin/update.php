<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Редактирование профиля</h1>

        <?$this->widget('zii.widgets.CBreadcrumbs', array(
            'htmlOptions' => array(
                'class' => 'breadcrumb'
            ),
            'separator' => ' / ',
            'links' => array(
                'Администрирование' => array('../admin'),
                'Пользователи' => array('../admin/user'),
                'Пользователь #' . $user->idUser
            )
        ));?>


        <? $form = $this->beginWidget('CActiveForm', array(
                'id' => 'edit-form',
                'action' => Yii::app()->request->baseUrl . '/admin/user/',
                'method' => 'post',
                'enableAjaxValidation' => true,
                'enableClientValidation' => true,
                'clientOptions' => array(
                    'validateOnSubmit' => true,
                    'validateOnChange' => true,
                )
            )
        )?>
        <?= $form->errorSummary($user, 'Пожалуйста, исправьте следующие ошибки:', '', array('class' => 'alert alert-danger')) ?>
        <?= CHtml::hiddenField('idUser', Yii::app()->getRequest()->getParam('id')); ?>
        <div class="form-group">
            <?= $form->labelEx($user, 'userFirstName') ?>
            <?= $form->textField($user, 'userFirstName', array('class' => 'form-control')) ?>
            <?= $form->error($user, 'userFirstName') ?>
        </div>
        <div class="form-group">
            <?= $form->labelEx($user, 'userLastName') ?>
            <?= $form->textField($user, 'userLastName', array('class' => 'form-control')) ?>
            <?= $form->error($user, 'userLastName') ?>
        </div>
        <div class="form-group">
            <?= $form->labelEx($user, 'userBalance') ?>
            <?= $form->numberField($user, 'userBalance', array('class' => 'form-control')) ?>
            <?= $form->error($user, 'userBalance') ?>
        </div>
        <div class="form-group">
            <?= $form->labelEx($user, 'role') ?>
            <?= $form->dropDownList($user, 'role', array('Администраторы' => 'Администраторы', 'Пользователи' => 'Пользователи'), array('class' => 'form-control')) ?>
            <?= $form->error($user, 'role') ?>
        </div>
        <?= CHtml::tag('button', array('class' => 'btn btn-default', 'type' => 'submit'), '<i class="fa fa-check fa-lg"></i> Обновить'); ?>
        <?= CHtml::tag('a', array('class' => 'btn btn-default', 'href' => '/admin/user'), '<i class="fa fa-close fa-lg"></i> Назад'); ?>
        <? $this->endWidget(); ?>

    </div>
</div>
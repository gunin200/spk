<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Пользователь #<?=$user->idUser?></h1>
    </div>
</div>

<?$this->widget('zii.widgets.CBreadcrumbs', array(
  'htmlOptions'=>array(
    'class'=>'breadcrumb'
    ),
  'separator'=>' / ',
  'links'=>array(
    'Администрирование'=>array('../admin'),
    'Пользователи'=>array('../admin/user'),
    'Пользователь #'.$user->idUser,
  )
));?>

<? if(Yii::app()->user->hasFlash('status')): ?>
    <div class="alert alert-<?=$status?>" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <?= Yii::app()->user->getFlash('status'); ?>
    </div>
<? endif; ?>

<?php $this->widget('zii.widgets.CDetailView', array(
    'data'=>User::model()->findByPk($user->idUser),
    'attributes'=>array(
        'idUser',
        'userFirstName',
        'userLastName',
        'userBalance',
        'role',
        ),
    )
); ?>
<?php

class DefaultController extends CController
{
    public function actionIndex()
    {
        if (!Yii::app()->user->isGuest) {
            $user = new User;
            $user->idUser = Yii::app()->user->id;
            $userInfo = $user->findById();
            if ($userInfo !== false) {
                $act = Yii::app()->getRequest()->getParam('act');
                $ticket = new Ticket;
                $ticketMessage = new TicketMessage;

                if ($act == 'add' && isset($_GET['Ticket'])) {
                    // Создаем заявку
                    $ticket = new Ticket;
                    $this->performAjaxValidation($ticket, 'ticket-form');
                    $ticket->subject = htmlspecialchars($_GET['Ticket']['subject']);
                    $ticket->who = Yii::app()->user->id;
                    $ticket->date = date('Y-m-d H:i:s');
                    $ticket->status = 2;
                    if ($ticket->validate()) {
                        if ($ticket->check() < 3) {
                            $ticket->status = 0;
                            $ticket->updateStatus();
                            $ticket->save();
                            $ticketMessage = new TicketMessage;
                            $ticketMessage->text = htmlspecialchars($_GET['TicketMessage']['text']);
                            $ticketMessage->who = Yii::app()->user->id;
                            $ticketMessage->ticket_id = $ticket->id;
                            $ticketMessage->date = date('Y-m-d H:i:s');
                            if ($ticketMessage->validate()) {
                                $ticketMessage->save();
                                Yii::app()->user->setFlash('status', 'Тикет успешно создан');
                            } else Yii::app()->user->setFlash('status', 'Не все данные заполнены или заполнены не верно');
                        } else Yii::app()->user->setFlash('status', 'Нельзя отправлять более 2 сообщений в течение 30 минут');
                    } else Yii::app()->user->setFlash('status', 'Не все данные заполнены или заполнены не верно');
                    $this->redirect(Yii::app()->getRequest()->getBaseUrl(true) . '/ticket');
                } else if ($act == 'close') {
                    $ticket = Ticket::model()->findByAttributes(array('id' => Yii::app()->getRequest()->getParam('id')));
                    $ticket->status = 3;
                    $ticketClose = $ticket->findById();
                    if ($ticketClose !== false) {
                        $who = false;
                        foreach ($ticketClose as $val) {
                            if ($val->who == Yii::app()->user->id) $who = true;
                        }
                        if ($who == true) {
                            if ($ticket->updateStatus()) Yii::app()->user->setFlash('statusClose', 'Успешно закрыли');
                            else Yii::app()->user->setFlash('statusClose', 'Не все данные заполнены или заполнены не верно');
                        } else Yii::app()->user->setFlash('statusClose', 'Не все данные заполнены или заполнены не верно.');
                    }
                    $this->redirect(Yii::app()->getRequest()->getBaseUrl(true) . '/ticket');
                }

                if ($act == 'view') {
                    $type = Yii::app()->getRequest()->getParam('type');
                    $ticket = Ticket::model()->findByPk(Yii::app()->getRequest()->getParam('id'));
                    $ticketInfo = $ticket->findById();
                    if ($ticket->who == Yii::app()->user->id) {
                        if ($type == 'add') {
                            if ($ticketInfo !== false) {
                                $text = new TicketMessage;
                                $this->performAjaxValidation($text, 'ticket-message');
                                $text->ticket_id = Yii::app()->getRequest()->getParam('id');
                                $text->text = htmlspecialchars($_POST['TicketMessage']['text']);
                                $text->who = Yii::app()->user->id;
                                $text->date = date('Y-m-d H:i:s');
                                if ($text->validate()) {
                                    if ($text->check() < 3) {
                                        $ticket->status = 0;
                                        $ticket->updateStatus();
                                        $ticket->save();
                                        $text->save();

                                        Yii::app()->user->setFlash('status', 'Успешно отправили');
                                    } else Yii::app()->user->setFlash('status', 'Нельзя отправлять более 2 сообщений в течение 30 минут.');
                                } else Yii::app()->user->setFlash('status', 'Не все данные заполнены или заполнены не верно');
                                $this->redirect(Yii::app()->getRequest()->getBaseUrl(true) . '/ticket/default/index/act/view/id/' . $text->ticket_id);
                            }
                        }
                        $dataProvider = new CActiveDataProvider('TicketMessage', array(
                            'criteria' => array(
                                'condition' => 'ticket_id = :id',
                                'params' => array(
                                    ':id' => Yii::app()->getRequest()->getParam('id')
                                )
                            )
                        ));
                        $this->render('ticketView', array(
                                'model' => $ticketMessage,
                                'status' => $ticket->getStatus(),
                                'id' => $ticket->id,
                                'name' => $ticket->subject,
                                'dataProvider' => $dataProvider,
                            )
                        );
                    } else {
                        $this->redirect(Yii::app()->getRequest()->getBaseUrl(true) . '/ticket');
                    }
                } else {
                    $dataProvider = new CActiveDataProvider('Ticket', array(
                        'criteria' => array(
                            'with' => 'user',
                            'condition' => 't.who = :id and t.who = user.idUser',
                            'params' => array(
                                ':id' => Yii::app()->user->id
                            ),
                            'order' => 't.date desc',
                            'group' => 't.id'
                        )
                    ));
                    $this->render('ticket', array(
                            'dataProvider' => $dataProvider,
                            'ticket' => $ticket,
                            'ticketMessage' => $ticketMessage
                        )
                    );
                }
            } else {
                $this->redirect(Yii::app()->request->getBaseUrl(true) . '/logout/');
            }
        } else {
            $this->redirect(Yii::app()->request->getBaseUrl(true));
        }
    }

    protected function performAjaxValidation($model, $form)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === $form) {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionInstall()
    {
        Yii::app()->db->createCommand()->createTable('ticket', array(
            'id' => 'string',
            'subject' => 'string',
            'who' => 'int(20)',
            'status' => 'int(1)',
            'timestamp' => 'varchar(20)',
        ), 'ENGINE=InnoDB');

        Yii::app()->db->createCommand()->createIndex('id_index', 'ticket', 'id');
        Yii::app()->db->createCommand()->createIndex('who_index', 'ticket', 'who');
        Yii::app()->db->createCommand()->createIndex('timestamp_index', 'ticket', 'timestamp');
    }
}
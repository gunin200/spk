<?php

/**
 * This is the model class for table "user".
 *
 * The followings are the available columns in table 'user':
 * @property integer $idUser
 * @property string $userFirstName
 * @property string $userLastName
 * @property double $userBalance
 */
class User extends CActiveRecord
{
    public static function avatar($id)
    {
        $response = SendHelper::send('https://api.vk.com/method/users.get?user_ids=' . $id . '&fields=photo_200&name_case=nom', null, null, null, true);
        $response = json_decode($response, true);
        if (isset($response['reponse']))
            return $response['response'][0]['photo_200'];
    }

    public static function Data($type)
    {
        $result = User::model()->find('idUser=:myParams', array(':myParams' => Yii::app()->user->id));
        if ($type == 'name') {
            return $result['userFirstName'] . ' ' . $result['userLastName'];
        } elseif ($type == 'balance') {
            $res = '';
            $rem = '';
            if ($result->typeSubs > 0) {
                $end = strtotime($result->datePay);
                $now = strtotime(date('Y-m-d H:i:s'));
                $second = $end - $now;
                $day = floor($second / 86400);
                $second -= $day * 86400;
                $hours = floor($second / 3600);
                $second -= $hours * 3600;
                $minutes = floor($second / 60);
                $second -= $minutes * 60;
                if ($day > 0) $rem .= $day . ' дн. ';
                if ($hours > 0) $rem .= $hours . ' ч. ';
                if ($minutes > 0) $rem .= $minutes . ' мин. ';
                if ($second > 0) $rem .= $second . ' с. ';
                $res .= 'Подписка истекает через: ' . $rem;
            } else $res .= 'Подписка истекла! <a href="/payment">Продлить</a> ';

            return $res . ' Баланс: ' . $result['userBalance'];
        }
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return User the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public static function isPremium()
    {
        $user = User::model()->findByPk(Yii::app()->user->id);
        if ($user->typeSubs) return true;
        else return false;
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'user';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('userFirstName, userLastName', 'required'),
            array('userBalance', 'numerical'),
            array('userFirstName, userLastName, role', 'length', 'max' => 255),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('idUser, userFirstName, test,typeSubs,datePay,userLastName, userBalance, role', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {

        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'Settings' => array(self::HAS_ONE, 'Settings', 'idUser'),
            'pay' => array(self::HAS_MANY, 'Pay', 'user_id'),
            'ticket' => array(self::HAS_MANY, 'Ticket', 'who')
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'idUser' => 'ID VK',
            'userFirstName' => 'Имя',
            'userLastName' => 'Фамилия',
            'userBalance' => 'Баланс',
            'role' => 'Группа',
            'test' => 'тест',
            'typeSubs' => 'тип',
            'datePay' => 'дата',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('idUser', $this->idUser);
        $criteria->compare('userFirstName', $this->userFirstName, true);
        $criteria->compare('userLastName', $this->userLastName, true);
        $criteria->compare('userBalance', $this->userBalance);
        $criteria->compare('role', $this->role);
        $criteria->compare('test', $this->test);
        $criteria->compare('typeSubs', $this->typeSubs);
        $criteria->compare('datePay', $this->datePay);



        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function findById()
    {
        $user = User::model()->findByAttributes(array('idUser' => $this->idUser));
        if ($user === null) {
            return false;
        } else {
            return $user;
        }
    }

    public function beforeSave()
    {
        if ($this->isNewRecord) {
            $statistics = Statistics::model()->find('date = :date', array(':date' => date('Y-m-d')));
            $statistics->user += 1;
            $statistics->save();
        }
        return true;
    }
}

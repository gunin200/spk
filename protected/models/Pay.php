<?php

/**
 * This is the model class for table "pay".
 *
 * The followings are the available columns in table 'pay':
 * @property integer $id
 * @property integer $user_id
 * @property integer $type
 * @property double $sum
 * @property string $date
 */
class Pay extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Pay the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pay';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
            array('user_id, type, type_pay, sum, date', 'required'),
            array('user_id, type, type_pay', 'numerical', 'integerOnly' => true),
			array('sum', 'numerical'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
            array('id, user_id, type, type_pay, sum, date', 'safe', 'on' => 'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'user' => array(self::BELONGS_TO, 'User', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'Код операции',
			'user_id' => 'Имя пользователя',
			'type' => 'Тип операции',
            'type_pay' => 'Тип оплаты',
			'sum' => 'Сумма операции',
			'date' => 'Дата операции',
		);
	}

    public function beforeSave() {
        $statistics = Statistics::model()->find('date = :date', array(':date'=>date('Y-m-d')));
        if ($this->type == 0) $statistics->moneyDown += $this->sum;
        elseif ($this->type == 1) $statistics->moneyUp += $this->sum;
        $statistics->save();
        return true;
    }

	/**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
	 */
    public function search()
	{
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('type', $this->type, true);
        $criteria->compare('type_pay', $this->type_pay);
        $criteria->compare('sum', $this->sum);

        if ($this->date != '') {
            $a = $this->date;
            $a = explode('/', $a);
            $from = trim($a[0]) . ' 00:00:00';
            $to = trim($a[1]) . ' 23:59:59';
            $criteria->addBetweenCondition('date', $from, $to);
        }

        if ($this->user_id != '') {
            $name = explode(' ', $this->user_id);
            $result = array();
            $criteriaUser = new CDbCriteria;
            if (count($name) == 1) {
                $criteriaUser->condition = 'userFirstName LIKE :ln OR userLastName LIKE :ln';
                $criteriaUser->params = array(':ln' => '%' . $name[0] . '%');
            } elseif (count($name) == 2) {
                $criteriaUser->condition = 'userFirstName LIKE :ln OR userLastName LIKE :fn OR
                userFirstName LIKE :fn OR userLastName LIKE :ln';
                $criteriaUser->params = array(':ln' => '%' . $name[0] . '%', ':fn' => '%' . $name[1] . '%');
            }
            $model = User::model()->findAll($criteriaUser);
            foreach ($model as $key => $value)
                $result[] = $value->idUser;
            unset($criteriaUser);
            unset($model);
            $criteria->addInCondition('user_id', $result);
        }

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => 500
            ),
        ));
	}
}

<?php

/**
 * This is the model class for table "userGroup".
 *
 * The followings are the available columns in table 'userGroup':
 * @property integer $idUser
 * @property string $url
 */
class UserGroup extends CActiveRecord
{
    public static function listGroup()
    {
        $response = UserGroup::model()->findAll('idUser = :id', array(':id' => Yii::app()->user->id));
        $list = '';
        for ($i = 0; $i < count($response); $i++) {
            $list .= '
			<li class="dlina">
				<a style="width:200px;float:left;word-break:break-all;" href="site/group?url=' . $response[$i]['url'] . '">' . $response[$i]['nameUrl'] . '</a>
				<i onclick="location.href=\'site/deletegroup?url=' . $response[$i]['url'] . '\'" class="fa fa-trash-o fa-lx" style="color:white;margin:13px 0 13px 5px;cursor:pointer;">
				</i>
			</li>';
        }
        return $list;
    }

	/**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return UserGroup the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'userGroup';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
            array('idUser, url, nameUrl', 'required', 'message' => 'Поле {attribute} является обязательным'),
			array('idUser', 'numerical', 'integerOnly'=>true),
            array('url, nameUrl', 'length', 'max' => 200, 'tooLong' => 'Поле {attribute} не может превышать 200 символов'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('idUser, url,nameUrl', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idUser' => 'Id User',
			'url' => 'Ссылка на группу',
			'nameUrl' => 'Название группы',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idUser',$this->idUser);
		$criteria->compare('url',$this->url,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}




}

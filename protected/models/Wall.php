<?php

/**
 * This is the model class for table "wall".
 *
 * The followings are the available columns in table 'wall':
 * @property integer $idWall
 * @property string $dataWall
 * @property string $textWall
 * @property string $photoWall
 * @property integer $idUser
 */
class Wall extends CActiveRecord
{
    public static function getStatistics($url)
    {
        preg_match('/vk.com\/(.*?)$/', $url, $matches);
        $result = '<tr>
                        <td><label>Альбомов: </label></td>
                        <td>0</td>
                    </tr>
                    <tr>
                        <td><label>Комментариев: </label></td>
                        <td>0</td>
                    </tr>
                    <tr>
                        <td><label>Спам: </label></td>
                        <td>0</td>
                    </tr>
                    <tr>
                        <td><label>Прочитанных: </label></td>
                        <td>0</td>
                    </tr>
                    <tr>
                        <td><label>Не прочитанных: </label></td>
                        <td>0</td>
                    </tr>';
        if (isset($matches[1])) {
            $response = SendHelper::send('https://api.vk.com/method/utils.resolveScreenName?screen_name=' . $matches[1], '', '', '', true);
            $response = json_decode($response, 1);
            if (isset($response['response']['object_id'])) {
                $idGroup = $response['response']['object_id'];
                $settings = Settings::model()->find('idUser = :id', array(':id' => Yii::app()->user->id));
                $statistics = array();
                $criteria = new CDbCriteria;
                $criteria->select = 'idAlbum, tipeSelect, COUNT(idAlbum) AS textWall';
                $criteria->condition = 'id_group = :gid and idSettings = :sid and idAlbum <> 0';
                $criteria->params = array(':gid' => $idGroup, ':sid' => $settings->idSettings);
                $criteria->group = 'idAlbum, tipeSelect';
                $wall = Wall::model()->findAll($criteria);

                if (count($wall)) {
                    foreach ($wall as $key => $value) {
                        $statistics['album'][$value['idAlbum']] = 1;
                        if (isset($statistics[$value['tipeSelect']]))
                            $statistics[$value['tipeSelect']] += $value['textWall'];
                        else
                            $statistics[$value['tipeSelect']] = $value['textWall'];
                    }
                    if (!isset($statistics[0])) $statistics[0] = 0;
                    if (!isset($statistics[1])) $statistics[1] = 0;
                    if (!isset($statistics[2])) $statistics[2] = 0;
                    if (!isset($statistics[3])) $statistics[3] = 0;
                    if (!isset($statistics['album'])) $statistics['album'] = 0;

                    $statistics['totalComment'] = $statistics[0] + $statistics[1] + $statistics[2] + $statistics[3];
                    $statistics['totalAlbum'] = count($statistics['album']);

                    $result = '
                    <tr>
                        <td><label>Альбомов: </label></td>
                        <td>' . $statistics['totalAlbum'] . '</td>
                    </tr>
                    <tr>
                        <td><label>Комментариев: </label></td>
                        <td>' . $statistics['totalComment'] . '</td>
                    </tr>
                    <tr>
                        <td><label>Спам: </label></td>
                        <td>' . $statistics[1] . '</td>
                    </tr>
                    <tr>
                        <td><label>Прочитанных: </label></td>
                        <td>' . $statistics[2] . '</td>
                    </tr>
                    <tr>
                        <td><label>Отмеченных: </label></td>
                        <td>' . $statistics[3] . '</td>
                    </tr>';
                }
            }
        }
        return $result;
    }

	/**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Wall the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'wall';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('dataWall', 'required'),
			array('idUser', 'numerical', 'integerOnly'=>true),
			array('photoWall', 'length', 'max'=>200),
			array('dataWall', 'date', 'format'=>'yyyy-MM-dd HH:mm:ss'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('idWall, dataWall, textWall, photoWall', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array('Settings'=>array(self::BELONGS_TO, 'Settings', 'idSettings'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idWall' => 'Идентификатор записи',
			'dataWall' => 'Дата записи',
			'textWall' => 'Текст записи',
			'photoWall' => 'Фото записи',
			'idUser' => 'Идентификатор пользователя',
			'idSettings' =>'Идентификатор настройки',
			'tipeText' => 'Тип записи',
			'idAlbum' => 'Идентификатор настройки',
			'nameAlbum' => 'Имя альбома',
			'id_group' => 'id Альбома',
			'nameUser' => 'Имя пользователя',
            'tipeSelect' => 'Тип',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idWall',$this->idWall);
		$criteria->compare('dataWall',$this->dataWall,true);
		$criteria->compare('textWall',$this->textWall,true);
		$criteria->compare('photoWall',$this->photoWall,true);
		$criteria->compare('idUser',$this->idUser);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}

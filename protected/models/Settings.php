<?php

/**
 * This is the model class for table "settings".
 *
 * The followings are the available columns in table 'settings':
 * @property integer $idSettings
 * @property integer $idUser
 * @property integer $idCountry
 * @property integer $idCity
 * @property integer $Sex
 * @property integer $ageFrom
 * @property integer $ageTo
 * @property string $dateFrom
 * @property string $dateTo
 * @property string $keyWord
 */
class Settings extends CActiveRecord
{
    Public $nameCity;

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Settings the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'settings';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('idUser, idCountry,  keyWord', 'required'),
            array('idUser, idCountry, idCity, Sex, ageFrom, ageTo', 'numerical', 'integerOnly' => true),
            array('nameCity, dateFrom, dateTo,ageFrom, ageTo', 'length', 'allowEmpty' => true),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('idSettings, idUser, idCountry, idCity, Sex, ageFrom, ageTo, dateFrom, dateTo, keyWord', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {

        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array('User' => array(self::BELONGS_TO, 'User', 'idUser'),
            'Wall' => array(self::HAS_MANY, 'Wall', 'idSettings')
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'idSettings' => 'Идентификатор настройки',
            'idUser' => 'Идентификатор пользователя',
            'idCountry' => 'Идентификатор страны',
            'idCity' => 'Идентификатор города',
            'Sex' => 'Пол',
            'ageFrom' => 'Возраст от',
            'ageTo' => 'Возраст до',
            'dateFrom' => 'Дата с',
            'dateTo' => 'Дата по',
            'keyWord' => 'Ключевые слова',
            'nameCity' => 'Название города',
            'countWall' => 'Колличество записей',
            'nowCount' => 'Количество пройденных записей',
            'dateTxt' => 'Время записи txt файла',
            'dateXls' => 'Время записи xls файла',
            'nameTxt' => 'имя текстового файла',
            'nameXls' => 'имя файла xls',
            'countComment' => 'Число комментариев',
            'maxComment' => 'Максимальное число комментариев',
            'nowComment' => 'Текущее число комментариев',
            'pidComment' => 'pid процесса',
            'pidWall' => 'пид стены',
            'DateWallFrom' => 'дата начала',
            'DateWallTo' => 'дата конца',
            'postImg' => 'Посты с картинками',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('idSettings', $this->idSettings);
        $criteria->compare('idUser', $this->idUser);
        $criteria->compare('idCountry', $this->idCountry);
        $criteria->compare('idCity', $this->idCity);
        $criteria->compare('Sex', $this->Sex);
        $criteria->compare('ageFrom', $this->ageFrom);
        $criteria->compare('ageTo', $this->ageTo);
        $criteria->compare('dateFrom', $this->dateFrom, true);
        $criteria->compare('dateTo', $this->dateTo, true);
        $criteria->compare('keyWord', $this->keyWord, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }
}
